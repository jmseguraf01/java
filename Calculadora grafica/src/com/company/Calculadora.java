package com.company;

import javax.swing.*;
import javax.swing.plaf.synth.SynthOptionPaneUI;
import javax.xml.stream.Location;
import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.EventListener;


public class Calculadora {
    private JTextField pantalla;
    private JButton a1Button;
    private JButton a2Button;
    private JButton a3Button;
    private JButton button4;
    private JButton a4Button;
    private JButton a5Button;
    private JButton a6Button;
    private JButton button8;
    private JButton a7Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton xButton;
    private JButton BORRARButton;
    private JButton borrarButton;
    private JButton button16;
    private JButton button17;
    private JButton a0Button;
    private JButton button19;
    private JButton button20;
    private JPanel calculatorInterface;


    public Calculadora() {
        // Defino el tamaño de la interfaz
        
        calculatorInterface.setPreferredSize(new Dimension(380,450));
        // Operador para hacer los calculos
        final String[] operador = {""};
        final String[] Numero1 = {""};

        // NUMERO 0
        a0Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Cojo el texto que hay en la pantalla
                String textoPantalla = pantalla.getText();
                // Escribo en pantalla el texto que habia mas el numero del boton
                pantalla.setText(textoPantalla + a0Button.getText());
            }
        });

        // NUMERO 1
        a1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a1Button.getText());
            }
        });
        // NUMERO 2
        a2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a2Button.getText());
            }
        });
        // NUMERO 3
        a3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a3Button.getText());
            }
        });

        // NUMERO 4
        a4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a4Button.getText());
            }
        });

        // NUMERO 5
        a5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a5Button.getText());
            }
        });


        // NUMERO 6
        a6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a6Button.getText());
            }
        });


        // NUMERO 7
        a7Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a7Button.getText());
            }
        });

        // NUMERO 8
        a8Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a8Button.getText());
            }
        });

        // NUMERO 9
        a9Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                pantalla.setText(textoPantalla + a9Button.getText());
            }
        });

        // COMA
        button19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String textoPantalla = pantalla.getText();
                //pantalla.setText(textoPantalla.split(.));
            }
        });


        // Borrar todo el texto de la pantalla
        BORRARButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                pantalla.setText("");
            }
        });

        // Borro el ultimo numero
        borrarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Borro el ultimo caracter de la pantalla
                pantalla.setText(pantalla.getText().substring(0, pantalla.getText().length()-1));
            }
        });

        // SUMA
        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               // Le asignoe el string de los numeros que hay en pantalla antes de darle al sumar
               Numero1[0] = pantalla.getText();
               operador[0] = "+";
               pantalla.setText(Numero1[0] + operador[0]);
            }
        });

        // RESTA
        button8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Numero1[0] = pantalla.getText();
                operador[0] = "-";
                pantalla.setText(Numero1[0] + operador[0]);
            }
        });

        // MULTPLICACION
        xButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Numero1[0] = pantalla.getText();
                operador[0] = "x";
                pantalla.setText(Numero1[0] + operador[0]);
            }
        });

        // DIVISION
        button16.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Numero1[0] = pantalla.getText();
                operador[0] = "/";
                pantalla.setText(Numero1[0] + operador[0]);
            }
        });

        // RESULTADO
        button20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Si se ha introducido la operacion escribiendo numeros con el teclado
                if (operador[0] == "") {
                    String todoTextoPantalla = pantalla.getText();
                    String operacion = "";
                    // Miro que operacion debo hacer
                    if (todoTextoPantalla.contains("+")) operacion = "+";
                    else if (todoTextoPantalla.contains("-")) operacion = "-";
                    else if (todoTextoPantalla.contains("x")) operacion = "x";
                    else if (todoTextoPantalla.contains("*")) operacion = "*";
                    else if (todoTextoPantalla.contains("/")) operacion = "/";
                    // Remplazo el simbolo de la operacion por un -
                    todoTextoPantalla = todoTextoPantalla.replace(operacion, "-");
                    // Divido el texto en 2 a partir del -
                    String [] partesTextoPantalla = todoTextoPantalla.split("-");
                    String numero1 =  partesTextoPantalla[0];
                    String numero2 =  partesTextoPantalla[1];
                    // Paso a numeros enteros los dos numeros
                    float intNumero1 = Integer.parseInt(numero1);
                    float intNumero2 = Integer.parseInt(numero2);
                    // Hago las operaciones
                    // Cuando el operador es una suma
                    if (operacion == "+") {
                        pantalla.setText(String.valueOf(intNumero1 + intNumero2));
                    }

                    // Para la resta
                    else if (operacion == "-") {
                        pantalla.setText(String.valueOf(intNumero1 - intNumero2));
                    }

                    // Para la multiplicacion
                    else if (operacion == "x" || operacion == "*") {
                        pantalla.setText(String.valueOf(intNumero1 * intNumero2));
                    }

                    // Para la division
                    else if (operacion == "/") {
                        pantalla.setText(String.valueOf( intNumero1 / intNumero2));
                    }

                }

                // L O  N O R M A L
                // Si se ha introducido la operacion dandole a los botones
                else {
                    // Transformo el numero1 y elt texto en pantalla en int
                    float intNumero1 = Integer.parseInt(Numero1[0]);
                    // Remplazo el operador y el numero1 a sumar
                    String intNumero2 = pantalla.getText().replace(Numero1[0]+ operador[0], "");
                    float intTextoPantalla = Integer.parseInt(intNumero2);


                    // Cuando el operador es una suma
                    if (operador[0] == "+") {
                        pantalla.setText(String.valueOf(intNumero1 + intTextoPantalla));
                    }

                    // Para la resta
                    else if (operador[0] == "-") {
                        pantalla.setText(String.valueOf(intNumero1 - intTextoPantalla));
                    }

                    // Para la multiplicacion
                    else if (operador[0] == "x") {
                        pantalla.setText(String.valueOf(intNumero1 * intTextoPantalla));
                    }

                    // Para la division
                    else if (operador[0] == "/") {
                        pantalla.setText(String.valueOf(intNumero1 / intTextoPantalla));
                    }
                    // Dejo en blanco el operador para que la proxima vez no entre en el else
                    operador[0] = "";
                }
            }
        });

        // Funcion para cuando se pulsa una tecla en la pantalla
        pantalla.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent key) {
                // Si la tecla pulsada es el enter
                if (key.getKeyCode() == KeyEvent.VK_ENTER) {
                    // NO QUIERO REPETIR EL CODIGO... BUSCANDO SOLUCION
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Calculadora");
        frame.setContentPane(new Calculadora().calculatorInterface);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


}
