package com.mycompany.login;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.*;
import static java.awt.SystemColor.desktop;
import java.io.File;
import java.nio.file.*;
import javax.swing.JLabel;

public class Login extends javax.swing.JFrame {
    public Login() {
        initComponents();        
    }
    // Booleanos para mostrar o ocultar la carpeta
    boolean mostrarCarpeta = false;
    boolean ocultarCarpeta = false;
    // Defino los colores verde y rojo
    Color colorVerde = new Color(0,68,11);
    Color colorRojo = new Color(171,0,0);
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButtonEntrar = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jTextPasswd = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextUser = new javax.swing.JTextPane();
        jLabel4 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login");
        setBackground(new java.awt.Color(91, 193, 240));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setForeground(new java.awt.Color(91, 193, 240));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(29, 160, 245));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\jusef\\Desktop\\git\\java\\Login\\src\\main\\java\\com\\mycompany\\login\\Imagenes\\user.png")); // NOI18N

        jLabel2.setFont(new java.awt.Font("Alef", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("USUARIO");

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Alef", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("CONTRASEÑA");

        jButtonEntrar.setBackground(new java.awt.Color(204, 204, 204));
        jButtonEntrar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButtonEntrar.setForeground(new java.awt.Color(0, 0, 0));
        jButtonEntrar.setText("Entrar");
        jButtonEntrar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonEntrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEntrarActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(0, 0, 153));
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setText("Salir");
        jButton1.setToolTipText("");
        jButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, java.awt.Color.white, null, java.awt.Color.white));
        jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jTextPasswd.setBackground(new java.awt.Color(255, 255, 255));
        jTextPasswd.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jTextPasswd.setForeground(new java.awt.Color(0, 0, 0));
        jTextPasswd.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextPasswd.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextPasswdKeyPressed(evt);
            }
        });

        jTextUser.setBackground(new java.awt.Color(255, 255, 255));
        jTextUser.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jTextUser.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTextUserKeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTextUser);

        jLabel4.setForeground(new java.awt.Color(171, 0, 0));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap(229, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jButtonEntrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                .addComponent(jTextPasswd, javax.swing.GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
                                .addComponent(jScrollPane1)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextPasswd, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonEntrar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, 520));

        jMenuBar2.setBackground(new java.awt.Color(255, 255, 255));

        jMenu2.setBackground(new java.awt.Color(255, 255, 255));
        jMenu2.setForeground(new java.awt.Color(0, 0, 0));
        jMenu2.setText("Funciones");
        jMenu2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        jMenuItem3.setText("Ocultar archivos");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem3);

        jMenuItem4.setText("Mostrar archivos");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuBar2.add(jMenu2);

        setJMenuBar(jMenuBar2);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
   
    // Cuando se le da al boton de enter
    private void jButtonEntrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonEntrarActionPerformed
        login();
    }//GEN-LAST:event_jButtonEntrarActionPerformed

    // Boton salir
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton1ActionPerformed

    // Click boton de ocultar archivos
    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
       ocultarCarpeta = true;
       // Cambio el texto de la label que esta para eso
       jLabel4.setForeground(colorRojo);
       jLabel4.setText("Debes de iniciar sesión para ocultar los archivos!");
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    // Click boton mostrar archivos
    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
       mostrarCarpeta = true;
       // Cambio el texto de la label que esta para eso
       jLabel4.setForeground(colorRojo);
       jLabel4.setText("Debes de iniciar sesión para mostrar los archivos!");
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    // Cuando pulsamos una tecla en la caja del usuario
    private void jTextUserKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextUserKeyPressed
        // Si le damos a enter
        if (evt.getKeyCode() == 10) {
            login();
        }
    }//GEN-LAST:event_jTextUserKeyPressed

    // Cuando pulsamos una tecla en la caja de contraseña
    private void jTextPasswdKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextPasswdKeyPressed
        // Si le damos a enter
        if (evt.getKeyCode() == 10) {
            login();
        }
    }//GEN-LAST:event_jTextPasswdKeyPressed

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Login().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButtonEntrar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPasswordField jTextPasswd;
    private javax.swing.JTextPane jTextUser;
    // End of variables declaration//GEN-END:variables
        
    // Funcion para cuando el login es correcto
    void login() {
        // Encripto el usuario
        String userIntro = jTextUser.getText();
        String userEncrypted ="";
        // El hash del usuario real encriptado
        String userRealEncriptada = "kyGharBCp9BSe/mndJonhB2TYUe8Xq1Vz/kuVXtJhP7u7ey9D70uMjgTeavDCIhGRLMlQ3s1d9xQeYLuBMYNzQ==";
        try {
            // Encripto la contraseña introducida por el usuario y despues compruebo que esa contraseña encriptada es igual a la encriptacion del passwd real
            userEncrypted = Base64.getEncoder().encodeToString(MessageDigest.getInstance("sha-512").digest(("opensourceforever"+userIntro).getBytes()));
        }
        // Si da error al desencriptar el usuario
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        // Encripto la contraseña
        // Si el usuario y la contraseña es correcta
        String passwIntro = jTextPasswd.getText();
        String passwdEncrypted = "";
        String contrasenaRealEncriptada = "xW9YlYQZy9lyVxFjStxHYs+vIPDSmE/lTqk8/OJYWWjzp87BatUMzHKAXP70d3Lqk1tDqfGzoz5s1LaA3VmxFg==";
        try {
            // Encripto la contraseña introducida por el usuario y despues compruebo que esa contraseña encriptada es igual a la encriptacion del passwd real
            passwdEncrypted = Base64.getEncoder().encodeToString(MessageDigest.getInstance("sha-512").digest(("opensourceforever"+passwIntro).getBytes()));
        }
        // Si da error al desencriptar la contraseña
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        // Compruebo que el user y passwd introducidos por el usuario es igual que el encriptado
        if (userEncrypted.equals(userRealEncriptada) && passwdEncrypted.equals(contrasenaRealEncriptada)) {
            // Pongo el color verde
            jLabel4.setForeground(colorVerde);
            // Si se ha seleccionado ocultar carpeta 
            if (ocultarCarpeta == true) {
                // Oculto la carpeta "Todo"
                try {
                    Runtime.getRuntime().exec("attrib +s +h Todo");
                    ocultarCarpeta = false;
                    // Cambio el texto de la label que esta para eso
                    jLabel4.setText("Los archivos se han ocultado con éxito.");
                }
                // Si hay error
                catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            
            // Si se ha seleccionado mostrar carpeta
            else if (mostrarCarpeta == true) {
                // Muestro la carpeta "Todo"
                try {
                    Runtime.getRuntime().exec("attrib -s -h Todo");
                    mostrarCarpeta = false;
                    // Cambio el texto de la label que esta para eso
                    jLabel4.setText("Los archivos se han mostrado con éxito.");
                }
                // Si hay error
                catch (IOException ex) {
                    ex.printStackTrace();
                }        
            }
            
            // Si no se ha seleccionado ninguna funcion, se abre la carpeta Todo
            else {
                try {
                    Desktop desktop = Desktop.getDesktop();
                    File dirToOpen = null;
                    System.out.println(System.getProperty("user.dir"));
                    // Abro la carpeta
                    try {
                        dirToOpen = new File("Todo");
                        desktop.open(dirToOpen);
                        jLabel4.setText("Inicio de sesión con éxito.");
                        // Espero 1 segundo para los ordenadores lentos
                        Thread.sleep(1500);
                        System.exit(0);
                    } catch (IllegalArgumentException iae) {
                        javax.swing.JOptionPane.showMessageDialog(null, "Error al encontrar la carpeta.");
                    }
                    // Except del sleep
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
                
        } // Usuario y contraseña incorrecta
        else {
            // Cambio el texto de la label que esta para eso
            jLabel4.setForeground(colorRojo);
            jLabel4.setText("Usuario y/o contraseña incorrecta.");
        }
    }
}
