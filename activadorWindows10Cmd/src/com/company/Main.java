package com.company;

import com.sun.jdi.ThreadReference;

import java.io.*;
import java.util.Scanner;

/*
 * Pagina web de esto: https://www.tusequipos.com/2018/10/03/activar-windows-10-gratis-sin-programas/
 * */

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Bienvenido !\nRecuerda que debes de haber ejecutado este programa como ADMINISTRADOR.");
        System.out.println("");
        // Sleep
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        menu(scanner);
    }

    // Funcion del Menu
    static void menu(Scanner scanner) {
        // Clear
        clear();
        // Menu
        System.out.println();
        System.out.println("-------------------------");
        System.out.println(" ACTIVADOR DE WINDOWS 10 ");
        System.out.println("-------------------------");
        System.out.println("Acciones: ");
        System.out.println("\t0. Salir");
        System.out.println("\t1. Activar Windows");

        // Opcion
        System.out.print("Escoge la opcion: ");
        int opcion = scanner.nextInt();

        // Activar W10
        if (opcion == 1) {
            seleccionarVersion(scanner);
        }

        // Exit
        else if (opcion == 0){
            System.exit(0);
        }

        else {
            menu(scanner);
        }
    }

    // Funcion para seleccionar la version de Windows
    static void seleccionarVersion(Scanner scanner) {
        /*
         * CLAVES DE WINDOWS 10
         * --------------------
         */
        String claveW10Home = "TX9XD-98N7V-6WMQ6-BX7FG-H8Q99";
        String claveW10Pro = "W269N-WFGWX-YVC9B-4J6C9-T83GX";
        String claveW10Enterprise = "NPPR9-FWDCX-D2C8J-H872K-2YT43";
        String claveParaActivar = "";
        System.out.println("Detectando versión de Windows 10 de forma automatica. . . ");
        try {
            Thread.sleep(Long.parseLong("3000"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Se detecta que Version de W10 es, de forma automatica
        try {
            Process versionW10 = Runtime.getRuntime().exec("wmic os get Caption /value");
            // Analizo el resultado que me ha dado
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(versionW10.getInputStream()));
            String line;
            // Leo cada linea del resultado del comando para encontrar la version de windows
            while ((line = stdInput.readLine()) != null) {
                // Es W10 home
                if (line.contains("home") || line.contains("Home")) {
                    claveParaActivar = claveW10Home;
                }

                // Es W10 Pro
                else if (line.contains("pro") || line.contains("Pro")) {
                    claveParaActivar = claveW10Pro;
                }

                // Es W10 enterprise
                else if (line.contains("enterprise") || line.contains("Enterprise")) {
                    claveParaActivar = claveW10Enterprise;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Si no se ha detectado la version de W10 de forma automatica
        if (claveParaActivar.equals("")) {
            System.out.println("No se ha podido detectar la version de Windows que tiene su PC.");
            // Sleep
            try {
                Thread.sleep(Long.parseLong("2000"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // Clear
            clear();
            System.out.flush();
            System.out.println();
            System.out.println("-------------------------");
            System.out.println(" VERSIONES DE WINDOWS 10 ");
            System.out.println("-------------------------");
            System.out.println(" 1 - WINDOWS 10 HOME");
            System.out.println(" 2 - WINDOWS 10 PRO");
            System.out.println(" 3 - WINDOWS 10 ENTERPRISE");
            System.out.println("");
            System.out.print("Selecciona la version de Windows que tienes en el PC que deseas activar: ");
            int versionWindows10 = scanner.nextInt();

            // W10 Home
            if (versionWindows10 == 1) {
                System.out.println("version home");
                claveParaActivar = claveW10Home;
            }

            // W10 Pro
            else if (versionWindows10 == 2) {
                System.out.println("Version pro");
                claveParaActivar = claveW10Pro;
            }

            // W10 Enterprise
            else if (versionWindows10 == 3) {
                System.out.println("Version enterprise");
                claveParaActivar = claveW10Enterprise;
            }
        }


        // Hay que ponerlo siempre antes de cada instruccion en la cmd
        String cmd_c = "cmd /c ";
        // COMMANDOS PARA ACTIVAR W10
       try {
            Runtime.getRuntime().exec(cmd_c + "slmgr /ipk " + claveParaActivar);
            Runtime.getRuntime().exec(cmd_c + "slmgr /skms kms.digiboy.ir");
            Runtime.getRuntime().exec(cmd_c + "slmgr /ato");
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        }


        System.out.println("");
        System.out.println("----------------");
        System.out.println("  É X I T O !!! ");
        System.out.println("----------------");
        System.out.println("Se han ejecutado los comandos para activar windows. A continuación te saldrá una ventana, COMPRUEBA que ponga que Windows ESTA ACTIVADO.\nEn el caso de que no salga activado, vuelve a ejecutar el programa.");
        // Sleep 5 seconds
        try {
            Thread.sleep(Long.parseLong("8000"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Compruebo que W10 este activado
        try {
            Runtime.getRuntime().exec(cmd_c + "SLMGR -XPR ");
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Gracias por usar el programa!");
        // Sleep & exit
        try {
            Thread.sleep(Long.parseLong("5000"));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    static void clear() {
        for (int i = 0; i < 25 ; i++) {
            System.out.println("");
        }
    }
}
