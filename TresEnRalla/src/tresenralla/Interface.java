    package tresenralla;

import java.awt.Color;
import java.awt.Font;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author jusef
 */
public class Interface extends javax.swing.JFrame {
    public Interface() {
        initComponents();
        // Oculto la label del trofeo de cuando ganas
        labelTrofeo.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        boton1_1 = new javax.swing.JButton();
        boton1_2 = new javax.swing.JButton();
        boton1_3 = new javax.swing.JButton();
        boton2_1 = new javax.swing.JButton();
        boton2_2 = new javax.swing.JButton();
        boton2_3 = new javax.swing.JButton();
        boton3_1 = new javax.swing.JButton();
        boton3_2 = new javax.swing.JButton();
        boton3_3 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        LabelTurno = new javax.swing.JLabel();
        labelTrofeo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Juego");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Mongolian Baiti", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 153));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Tres en raya");

        jPanel2.setBackground(new java.awt.Color(153, 255, 255));

        boton1_1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton1_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1_1ActionPerformed(evt);
            }
        });

        boton1_2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton1_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1_2ActionPerformed(evt);
            }
        });

        boton1_3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton1_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1_3ActionPerformed(evt);
            }
        });

        boton2_1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton2_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2_1ActionPerformed(evt);
            }
        });

        boton2_2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton2_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2_2ActionPerformed(evt);
            }
        });

        boton2_3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton2_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2_3ActionPerformed(evt);
            }
        });

        boton3_1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton3_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3_1ActionPerformed(evt);
            }
        });

        boton3_2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton3_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3_2ActionPerformed(evt);
            }
        });

        boton3_3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        boton3_3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3_3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(boton3_1, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                    .addComponent(boton1_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton2_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton1_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton2_2, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                    .addComponent(boton3_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton1_3, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                    .addComponent(boton2_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton3_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(24, 24, 24))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(boton1_2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton1_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton1_3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton2_1, javax.swing.GroupLayout.DEFAULT_SIZE, 81, Short.MAX_VALUE)
                    .addComponent(boton2_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton2_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(boton3_1, javax.swing.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                    .addComponent(boton3_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(boton3_3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jButton10.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        jButton10.setForeground(new java.awt.Color(51, 102, 0));
        jButton10.setText("Jugar de nuevo");
        jButton10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 153));
        jLabel2.setText("Turno del jugador: ");

        LabelTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png"))); // NOI18N

        labelTrofeo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/trofeo.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(LabelTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(labelTrofeo))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(33, 33, 33)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(LabelTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(labelTrofeo, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 32, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 700, 400));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interface().setVisible(true);
            }
        });
    }
    
    // Variables 
    // --------------------
    /* Toca impar = X
       Toca par = O*/
    int toca = 1;
    String nombreBoton = "";
    // Defino los iconos de todo los botones para luego compararlos
    String buton1_1 = "";
    String buton1_2 = "";
    String buton1_3 = "";
    String buton2_1 = "";
    String buton2_2 = "";
    String buton2_3 = "";
    String buton3_1 = "";
    String buton3_2 = "";
    String buton3_3 = "";
    // ----------------------

    // Clicks en botones
    // -----------------------------------
    private void boton1_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton1_1ActionPerformed
        // Siempre que el boton no se haya clickado
        if (boton1_1.getIcon() == null) {
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton1_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            // Deshabilito el boton
            toca++;
            comprobarGanador();
        }

    }//GEN-LAST:event_boton1_1ActionPerformed

    private void boton1_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton1_2ActionPerformed
        if (boton1_2.getIcon() == null) {
            if (toca % 2 != 0) {
               boton1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton1_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();   
        }
    }//GEN-LAST:event_boton1_2ActionPerformed

    private void boton1_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton1_3ActionPerformed
        if (boton1_3.getIcon() == null) {
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton1_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton1_3ActionPerformed

    private void boton2_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton2_1ActionPerformed
        if (boton2_1.getIcon() == null) {
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton2_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton2_1ActionPerformed

    private void boton2_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton2_2ActionPerformed
        if (boton2_2.getIcon() == null) {        
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton2_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton2_2ActionPerformed

    private void boton2_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton2_3ActionPerformed
        if (boton2_3.getIcon() == null) {        
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton2_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton2_3ActionPerformed

    private void boton3_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton3_1ActionPerformed
        if (boton3_1.getIcon() == null) {        
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton3_1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton3_1ActionPerformed

    private void boton3_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton3_2ActionPerformed
        if (boton3_2.getIcon() == null) {        
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton3_2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton3_2ActionPerformed

    private void boton3_3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton3_3ActionPerformed
        if (boton3_3.getIcon() == null) {
            // Si es impar le toca a la X
            if (toca % 2 != 0) {
               boton3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
            } else {
               boton3_3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
            }
            toca++;
            comprobarGanador();
        }
    }//GEN-LAST:event_boton3_3ActionPerformed
    // FIN click en botones
    // -----------------------------------
    
    
    // Boton para jugar de nuevo
    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // Reinicio a quien le toca
        toca = 1;
        cambioTurno();
        boton1_1.setIcon(null);
        boton1_2.setIcon(null);
        boton1_3.setIcon(null);
        boton2_1.setIcon(null);
        boton2_2.setIcon(null);
        boton2_3.setIcon(null);
        boton3_1.setIcon(null);
        boton3_2.setIcon(null);
        boton3_3.setIcon(null);
        // Reincio las variables de si los botones tienen icono
        buton1_1 = "";
        buton1_2 = "";
        buton1_3 = "";
        buton2_1 = "";
        buton2_2 = "";
        buton2_3 = "";
        buton3_1 = "";
        buton3_2 = "";
        buton3_3 = "";
        boton1_1.setEnabled(true);
        boton1_2.setEnabled(true);
        boton1_3.setEnabled(true);
        boton2_1.setEnabled(true);
        boton2_2.setEnabled(true);
        boton2_3.setEnabled(true);
        boton3_1.setEnabled(true);
        boton3_2.setEnabled(true);
        boton3_3.setEnabled(true);
        labelTrofeo.setVisible(false);
        jLabel2.setText("Turno del jugador:");
        jLabel2.setFont(new Font(jLabel2.getName(), Font.PLAIN, 14));
        Color azulReinicio = new Color(0,0,153); 
        jLabel2.setForeground(azulReinicio);

        
    }//GEN-LAST:event_jButton10ActionPerformed
    

    // Funcion para comprobar a quien le toca y cambiar el turno
    void cambioTurno() {
        // Le toca a x
        if (toca % 2 != 0) {
            LabelTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/x.png")));
        } 
        // Le toca a 0
        else {
            LabelTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/tresenralla/Imagenes/0.png")));
        }
    }
    

    
    // Funcion que comprueba si hay algun ganador
    void comprobarGanador() {
        boolean gana = false;
        // Solo cojo la descripcion del icono cuando tiene un icono, y solo lo cojo 1 vez
        // siempre que no se haya cogido antes
        
        // Primera fila
        if (boton1_1.getIcon() != null && buton1_1.equals("")) {
            buton1_1 = ((ImageIcon)boton1_1.getIcon()).getDescription();
        } if (boton1_2.getIcon() != null && buton1_2.equals("")) {
            buton1_2 = ((ImageIcon)boton1_2.getIcon()).getDescription();    
        } if (boton1_3.getIcon() != null && buton1_3.equals("")) {
            buton1_3 = ((ImageIcon)boton1_3.getIcon()).getDescription();
        }
        // Segunda fila
        if (boton2_1.getIcon() != null && buton2_1.equals("")) {
            buton2_1 = ((ImageIcon)boton2_1.getIcon()).getDescription();
        } if (boton2_2.getIcon() != null && buton2_2.equals("")) {
            buton2_2 = ((ImageIcon)boton2_2.getIcon()).getDescription();
        } if (boton2_3.getIcon() != null && buton2_3.equals("")) {
            buton2_3 = ((ImageIcon)boton2_3.getIcon()).getDescription();
        }
        
        // Tercera fila
        if (boton3_1.getIcon() != null && buton3_1.equals("")) {
            buton3_1 = ((ImageIcon)boton3_1.getIcon()).getDescription();    
        } if (boton3_2.getIcon() != null && buton3_2.equals("")) {
            buton3_2 = ((ImageIcon)boton3_2.getIcon()).getDescription();
        } if (boton3_3.getIcon() != null && buton3_3.equals("")) {
            buton3_3 = ((ImageIcon)boton3_3.getIcon()).getDescription();    
        }
        

        // Compruebo si ha ganado
        // ----------------------
        // Primera fila
        // Siempre que sean iguales y sean diferentes a lo que viene por defecto
        if ((buton1_1.equals(buton1_2) && buton1_2.equals(buton1_3)) && (!buton1_1.equals("") && !buton1_2.equals("") && !buton1_3.equals(""))) {
            gana = true;
        }    

        // Segunda fila
        else if ( (buton2_1.equals(buton2_2) && buton2_2.equals(buton2_3)) && (!buton2_1.equals("") && !buton2_2.equals("") && !buton2_3.equals(""))) {
            gana = true;
        }    
        
        // Tercera fila
        else if ( (buton3_1.equals(buton3_2) && buton3_2.equals(buton3_3)) && (!buton3_1.equals("") && !buton3_2.equals("") && !buton3_3.equals(""))) {
            gana = true;
        }
        
        // Primera columna
        else if ( (buton1_1.equals(buton2_1) && buton2_1.equals(buton3_1)) && (!buton1_1.equals("") && !buton2_1.equals("") && !buton3_1.equals("")) ) {
            gana = true;
        }
        
        // Segunda columna
        else if ( (buton1_2.equals(buton2_2) && buton2_2.equals(buton3_2)) && (!buton1_2.equals("") && !buton2_2.equals("") && !buton3_2.equals(""))) {
            gana = true;
        }
        
        // Tercera columna
        else if ((buton1_3.equals(buton2_3) && buton2_3.equals(buton3_3)) && (!buton1_3.equals("") && !buton2_3.equals("") && !buton3_3.equals(""))) {
            gana = true;
        }
        
        // Diagonal de derecha a izquierda
        else if ((buton1_3.equals(buton2_2) && buton2_2.equals(buton1_3)) && (!buton1_3.equals("") && !buton2_2.equals("") && !buton3_1.equals(""))) {
            gana = true;
        }
        
        // Diagonal de izquierda a derecha
        else if ((buton1_1.equals(buton2_2) && buton2_2.equals(buton3_3)) && (!buton1_1.equals("") && !buton2_2.equals("") && !buton3_3.equals(""))) {
            gana = true;
        }
        
        // Ha ganado
        if (gana == true) {
            // Cambio las caracteristicas de la label del turno
            jLabel2.setText("Has ganado!");
            jLabel2.setFont(new Font(jLabel2.getName(), Font.PLAIN, 20));
            Color verdeGanador = new Color(51, 153, 51); 
            jLabel2.setForeground(verdeGanador);
            // Deshabilito todos los botones
            boton1_1.setEnabled(false);
            boton1_2.setEnabled(false);
            boton1_3.setEnabled(false);
            boton2_1.setEnabled(false);
            boton2_2.setEnabled(false);
            boton2_3.setEnabled(false);
            boton3_1.setEnabled(false);
            boton3_2.setEnabled(false);
            boton3_3.setEnabled(false);
            // Muestro la label de la foto del trofeo
            labelTrofeo.setVisible(true);
        }
        else {
            cambioTurno();    
        }
        
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelTurno;
    private javax.swing.JButton boton1_1;
    private javax.swing.JButton boton1_2;
    private javax.swing.JButton boton1_3;
    private javax.swing.JButton boton2_1;
    private javax.swing.JButton boton2_2;
    private javax.swing.JButton boton2_3;
    private javax.swing.JButton boton3_1;
    private javax.swing.JButton boton3_2;
    private javax.swing.JButton boton3_3;
    private javax.swing.JButton jButton10;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel labelTrofeo;
    // End of variables declaration//GEN-END:variables
    
}
