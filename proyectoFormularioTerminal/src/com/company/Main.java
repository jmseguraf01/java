package com.company;

import javax.swing.plaf.TableHeaderUI;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        clear();
        System.out.println("****************************************");
        System.out.println("Bienvenido al programa de formularios.");
        System.out.println("Creado por Juanmi y Ivan.");
        System.out.println("****************************************");
        System.out.println("          ..- - .              \n" +
                "                   '        `.           \n" +
                "                  '.- .  .--. .          \n" +
                "                 |: _ | :  _ :|          \n" +
                "                 |`(@)--`.(@) |          \n" +
                "                 : .'     `-, :          \n" +
                "                 :(_____.-'.' `          \n" +
                "                 : `-.__.-'   :          \n" +
                "                 `  _.    _.   .         \n" +
                "                /  /  `_ '  \\    .       \n" +
                "               .  :          \\\\   \\      \n" +
                "              .  : _      __  .\\   .     \n" +
                "             .  /             : `.  \\    \n" +
                "            :  /      '        : `.  .   \n" +
                "           '  `      :          : :  `.  \n" +
                "         .`_ :       :          / '   |  \n" +
                "         :' \\ .      :           '__  :  \n" +
                "      .--'   \\`-._    .      .' :    `).  \n" +
                "    ..|       \\   )          :   '._.'  : \n" +
                "   ;           \\-'.        ..:         / \n" +
                "   '.           \\  - ....-   |        '  \n" +
                "      -.         :   _____   |      .'   \n" +
                "        ` -.    .'--       --`.   .'     \n" +
                "            `--                --        ");
        System.out.println("\n\nCargando . . .");
        Thread.sleep(3000);
        MenuFormulario();
    }

    static void MenuFormulario() {
        Scanner scanner = new Scanner(System.in);
        // Variable para salir del programa
        boolean exit = false;
        // Siempre que exit sea false entra al menu
        while (exit != true){
            // Llamo a la funcion que limpia la pantalla
            clear();
            System.out.println("+-----------------------------------+");
            System.out.println("|    M E N U  P R I N C I P A L     |");
            System.out.println("+-----------------------------------+");
            System.out.println("|\t1. Formulario del Fifa.         |");
            System.out.println("|\t2. Formulario de Futbol.        |");
            System.out.println("|\t0. Salir.                       |");
            System.out.println("+----------------------------------+");
            System.out.println("");
            System.out.print("Selecciona la opcion que desees: ");
            // Variable con la opcion a elegir
            int opcion = scanner.nextInt();

            // Torneo del fifa
            if (opcion == 1) {
                FormularioTorneoFifa formularioTorneoFifa = new FormularioTorneoFifa();
                // Llamo a las funciones del formulario del fifa
                formularioTorneoFifa.menu();
                formularioTorneoFifa.imprimirForum();
            }

            // Formulario ded futbol
            else if (opcion == 2){
                // Creo la variable que llama a la clase FormularioFutbol
                FormularioFutbol formularioFutbol = new FormularioFutbol();
                // LLamo a la funcion formFutbol de la clase FormularioFutbol
                formularioFutbol.formFutbol();
                // Imprimo los datos del formulario de la clase FormularioFutbol
                formularioFutbol.imprimirForm();
            }

            // Salir
            else if ( opcion == 0){
                exit = true;
            }

            // Si se ha seleccionado una opcion erronea
            else {
                System.out.println("Seleccione una opcion correcta.\nCargando . . .");
                // Time sleep de 2 segundo
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException a) {
                    a.printStackTrace();
                }
            }
        }
    }

    // Funcion para imprimir 25 lineas en blanco y asi limpia la pantalla.
    static void clear(){
        for (int i = 0; i < 35; i++) {
            System.out.println("");
        }
    }
}
