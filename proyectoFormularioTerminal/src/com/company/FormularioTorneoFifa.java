package com.company;

import java.util.Scanner;

public class FormularioTorneoFifa {
    Scanner scanner = new Scanner(System.in);
    // Variables
    String nombre;
    String apellidos;
    int edad;
    String dni;
    String plataforma;
    String nick;
    boolean exit = false;

    // Funcion menu
    void menu() {
        System.out.println("");
        // Nombre
        System.out.print("Nombre: ");
        nombre = scanner.nextLine();

        // Apellido
        System.out.print("Apellidos: ");
        apellidos = scanner.nextLine();

        // edad
        System.out.print("Edad: ");
        edad = scanner.nextInt();
        scanner.nextLine();

        // dni
        System.out.print("DNI: ");
        dni = scanner.nextLine();

        // plataforma
        System.out.print("Plataforma (PS4/PC/PS3): ");
        plataforma = scanner.nextLine();

        // nick
        System.out.print("Nickname: ");
        nick = scanner.nextLine();
    }

    // Funcion para imprimir los datos
    void imprimirForum(){
        System.out.println("");
        System.out.println("---------------------");
        System.out.println("     Torneo Fifa     ");
        System.out.println("---------------------");
        System.out.println("\tNombre: " + nombre);
        System.out.println("\tApellidos: " + apellidos);
        System.out.println("\tEdad: " + edad);
        System.out.println("\tDNI: " + dni);
        System.out.println("\tPlataforma: " + plataforma);
        System.out.println("\tNickname: " + nick);
        System.out.println("");

        System.out.print("Desea guardar sus datos? (S/N): ");

        String decision = scanner.nextLine();

        if (decision.equals("si") || decision.equals("s") || decision.equals("S")) {
            System.out.println("Tus datos han sido guardados.");
            exit = true;
        } else {
            System.out.println("Tus datos no han sido guardados.");
            exit = false;
        }

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
