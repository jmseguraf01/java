package com.company;

import java.util.Scanner;

public class FormularioFutbol {
    Scanner scanner = new Scanner(System.in);
    // Defino las variables aquí para poder imprimirlas en todas las funciones
    String nombre;
    String primerApellido;
    String segundoApellido;
    int edad;
    String DNI;

    // Funcion para rellenar con los datos
    void formFutbol(){
        System.out.println("\n\n");
        System.out.println("Formulario de Futbol.");
        System.out.println("----------------------");
        System.out.println("");
        // Nombre
        System.out.print("Introduzca su nombre: ");
        nombre = scanner.nextLine();
        // Primer Apellido
        System.out.print("Introduzca el primer apellido: ");
        primerApellido = scanner.nextLine();
        // Segundo apellido
        System.out.print("Introduzca el segundo apellido: ");
        segundoApellido = scanner.nextLine();
        // Edad
        System.out.print("Indique su edad: ");
        edad = scanner.nextInt();
        // DNI
        System.out.print("Escriba su DNI (incluida letra): ");
        DNI = scanner.next();
    }

    // Imprimo por pantalla los datos introducidos anteriormente
    void imprimirForm() {
        System.out.println("\n\n----------------------------");
        System.out.println(" C.F.  S IN G U E R L I N   ");
        System.out.println("----------------------------");
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellidos: " + primerApellido + " " + segundoApellido);
        System.out.println("Edad: " + edad);
        System.out.println("DNI " + DNI);
        System.out.println("----------------------------");
        System.out.println("");
        System.out.print("Desea guardar los datos? (S/N): ");
        String guardar = scanner.next();
        // Guardar
        if (guardar.equals("S")|| guardar.equals("s")) {
            System.out.println("Datos guardados con éxito.");
        }
        // No guardar
        else if (guardar.equals("N") || guardar.equals("n")) {
            System.out.println("Datos no guardados.");
        }
        // Vuelvo al menu
        System.out.println("\nVolviendo al menu...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
