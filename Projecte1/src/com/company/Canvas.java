package com.company;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Canvas extends javax.swing.JPanel {
    static Canvas canvas;
    static List<Object> objects = new ArrayList<>();

    public Canvas() {
        JFrame frame = new JFrame();
        frame.add(this);
        frame.setSize(600, 450);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // Para aliniar en el centro el Jframe
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    @Override
    public void paint(Graphics g) {
        for(Object o: objects) try {
            Class c = o.getClass(); String n = c.getName();
            if(!n.endsWith(".Circle")&&!n.endsWith(".Rectangle")&&!n.endsWith(".Triangle"))continue;
            int x = (int) c.getDeclaredField("positionX").get(o);
            int y = (int) c.getDeclaredField("positionY").get(o);
            g.setColor((Color) c.getDeclaredField("color").get(o));
            if (n.endsWith(".Circle")) {
                int r = (int) c.getDeclaredField("radius").get(o);
                g.fillOval(x, y, r * 2, r * 2);
            } else {
                int w = (int) c.getDeclaredField("width").get(o);
                int h = (int) c.getDeclaredField("height").get(o);
                if (n.endsWith(".Rectangle")) g.fillRect(x, y, w, h);
                else g.fillPolygon(new int[]{x,x+w/2,x+w},new int[]{y+h,y,y+h},3);
            }
        } catch (Exception e){ e.printStackTrace(); }
    }

    public static void draw(Object c) {
        if(canvas==null) canvas = new Canvas();
        objects.add(c);
    }
}
