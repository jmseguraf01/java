package com.company;

import java.awt.*;

public class Persona {
    Color colorCara;
    Color colorOjos;
    Color colorBoca;
    Color colorPupilaIzq;
    Color colorPupilaDer;
    Color colorCuerpo;
    Color colorBrazoIzq;
    Color colorManoIzq;
    Color colorBrazoDer;
    Color colorManoDer;
    Color colorPiernaIzq;
    Color colorPieIzq;
    Color colorPiernaDer;
    Color colorPieDer;

    int size;
    int x, y;

    void draw(){
        // Dibujo la cara
        Circle cara = new Circle();
        cara.color = colorCara;
        cara.radius = size/3;
        cara.positionX = x+size/3;
        cara.positionY = y-size/2;
        // Mano derecha


        // Hojo izquierdo
        Circle leftEye = new Circle();
        leftEye.color = colorOjos;
        leftEye.radius = size/14;
        leftEye.positionX = x+size/2;
        leftEye.positionY = y-size/3;
        // Pupila izquierda
        Circle pupilaIzquierda = new Circle();
        pupilaIzquierda.color = colorPupilaIzq;
        pupilaIzquierda.radius = size/34;
        pupilaIzquierda.positionX = (x+size/3) + (size/5);
        pupilaIzquierda.positionY = (y-size/3) + (size/18);


        // Hojo derecho
        Circle rightEye = new Circle();
        rightEye.color = colorOjos;
        rightEye.radius = size/14;
        rightEye.positionX = (x+size/2) + (size/4);
        rightEye.positionY = y-size/3;
        // Pupila derecha
        Circle pupilaDerecha = new Circle();
        pupilaDerecha.color = colorPupilaDer;
        pupilaDerecha.radius = size/34;
        pupilaDerecha.positionX = (x+size/4) + (size/2) + (size/24);
        pupilaDerecha.positionY = (y-size/3) + (size/18);

        // El rectangulo es la boca
        Rectangle boca = new Rectangle();
        boca.color = colorBoca;
        boca.height = size/30;
        boca.width = size/5;
        boca.positionX = x/2 + size - size/11;
        boca.positionY = y;

        // El cuerpo de la persona
        Rectangle cuerpo = new Rectangle();
        cuerpo.color = colorCuerpo;
        cuerpo.height = size - size/3;
        cuerpo.width = size/3;
        cuerpo.positionX = x*2 - x/4;
        cuerpo.positionY = y + y/7;

        // Brazo izquierdo
        Rectangle brazoIzq = new Rectangle();
        brazoIzq.color = colorBrazoIzq;
        brazoIzq.height = size - size/3;
        brazoIzq.width = size/10;
        brazoIzq.positionX = x*2 - x/2;
        brazoIzq.positionY = y + y/7;
        // Mano izquierda
        Circle manoIzq = new Circle();
        manoIzq.color = colorManoIzq;
        manoIzq.radius = size/14;
        manoIzq.positionX = x*2 - x/2;
        manoIzq.positionY = y + y/2 + y/4;

        // Brazo derecho
        Rectangle brazoDer = new Rectangle();
        brazoDer.color = colorBrazoDer;
        brazoDer.height = size - size/3;
        brazoDer.width = size/10;
        brazoDer.positionX = x*2 + x/3;
        brazoDer.positionY = y + y/7;
        // Mano derecha
        Circle manoDer = new Circle();
        manoDer.color = colorManoDer;
        manoDer.radius = size/14;
        manoDer.positionX = x*2 + x/3;
        manoDer.positionY = y + y/2 + y/4;

        // Pierna izquierda
        Rectangle piernaIzq = new Rectangle();
        piernaIzq.color = colorPiernaIzq;
        piernaIzq.height = size/2;
        piernaIzq.width = size/15;
        piernaIzq.positionX = x*2 - x/5;
        piernaIzq.positionY = y + y/2 + y/3;
        // Pie izquierdo
        Circle pieIzq = new Circle();
        pieIzq.color = colorPieIzq;
        pieIzq.radius = size/16;
        pieIzq.positionX =  x*2 - x/5 - x/17;
        pieIzq.positionY = y*2 + y/2 - y/5;

        // Pierna derecha
        Rectangle piernaDer = new Rectangle();
        piernaDer.color = colorPiernaDer;
        piernaDer.height = size/2;
        piernaDer.width = size/15;
        piernaDer.positionX =  x*2 + x/5 - x/10;
        piernaDer.positionY = y + y/2 + y/3;
        // Pie derecho
        Circle pieDer = new Circle();
        pieDer.color = colorPieDer;
        pieDer.radius = size/16;
        pieDer.positionX =  x*2 + x/5 - x/10;
        pieDer.positionY = y*2 + y/2 - y/5;


        Canvas.draw(cara);
        Canvas.draw(leftEye);
        Canvas.draw(pupilaIzquierda);
        Canvas.draw(rightEye);
        Canvas.draw(pupilaDerecha);
        Canvas.draw(boca);
        Canvas.draw(cuerpo);
        Canvas.draw(brazoIzq);
        Canvas.draw(manoIzq);
        Canvas.draw(brazoDer);
        Canvas.draw(manoDer);
        Canvas.draw(piernaIzq);
        Canvas.draw(pieIzq);
        Canvas.draw(piernaDer);
        Canvas.draw(pieDer);

    }
}