package com.company;

import java.awt.*;

public class Pizarra {
    Color tablaColor;
    Color colorBorde;

    int size;
    int x, y;

    void draw(){
        Rectangle tabla = new Rectangle();
        tabla.color = tablaColor;
        tabla.width = size/2;
        tabla.height = size/4;
        tabla.positionX = x;
        tabla.positionY = y;

        Rectangle borde = new Rectangle();
        borde.color = colorBorde;
        borde.width = 30+size/2;
        borde.height = 20+size/4;
        borde.positionX = x-15;
        borde.positionY = y-10;

        Rectangle poyete = new Rectangle();
        poyete.color = colorBorde;
        poyete.width = 290-size/2;
        poyete.height = 120-size/4;
        poyete.positionX = 60+x;
        poyete.positionY = 110+y;

        Rectangle tizas = new Rectangle();
        tizas.color = Color.WHITE;
        tizas.width = 250-size/2;
        tizas.height = 108-size/4;
        tizas.positionX = 70+x;
        tizas.positionY = 110+y;

        Canvas.draw(borde);
        Canvas.draw(tabla);
        Canvas.draw(poyete);
        Canvas.draw(tizas);
    }
}
