package com.company;


import javax.swing.*;
import java.awt.*;
import java.beans.beancontext.BeanContextServiceAvailableEvent;

public class Main {
    public static void main(String[] args) {

        // Defino la persona
        Persona persona = new Persona();
        // Colores creados por mi
        Color BROWN = new Color(0x543300);
        Color CARNE = new Color(245, 219, 180);

        // Defino el tamaño y la x y de persona
        persona.size = 150;
        persona.x = 100;
        persona.y = 150;

        persona.colorCara = CARNE;
        persona.colorOjos = Color.BLACK;
        persona.colorBoca = Color.RED;
        persona.colorPupilaIzq = BROWN;
        persona.colorPupilaDer = BROWN;
        persona.colorCuerpo = BROWN;
        persona.colorBrazoIzq = BROWN;
        persona.colorManoIzq = CARNE;
        persona.colorBrazoDer = BROWN;
        persona.colorManoDer = CARNE;
        persona.colorPiernaIzq = BROWN;
        persona.colorPieIzq = Color.BLACK;
        persona.colorPiernaDer = BROWN;
        persona.colorPieDer = Color.BLACK;

        // Defino pizarra
        Pizarra pizarra = new Pizarra();
        pizarra.size = 410;
        pizarra.x = 320;
        pizarra.y = 100;

        pizarra.tablaColor = Color.WHITE;
        pizarra.colorBorde = BROWN;

        // Dibujo la persona
        persona.draw();
        // Dibujo pizarra
        pizarra.draw();

    }

    /*
    public static void mainHouse(String[] args) {

        Circle sun = new Circle();
        sun.radius = 40;
        sun.positionX = 440;
        sun.positionY = 50;
        sun.color = Color.YELLOW;

        Circle window = new Circle();
        window.color = Color.GRAY;
        window.positionX = 240;
        window.positionY = 215;
        window.radius = 10;

        Circle earth = new Circle();
        earth.color = Color.GREEN;
        earth.positionX = -300;
        earth.positionY = 250;
        earth.radius = 600;

        Rectangle wall = new Rectangle();
        wall.width = 100;
        wall.height = 80;
        wall.positionX = 200;
        wall.positionY = 200;
        wall.color = Color.WHITE;

        Rectangle sky = new Rectangle();
        sky.width = 600;
        sky.height = 400;
        sky.positionX = 0;
        sky.positionY = 0;
        sky.color = Color.CYAN;

        Triangle roof = new Triangle();
        roof.width = 100;
        roof.height = 50;
        roof.positionX = 200;
        roof.positionY = 150;
        roof.color = Color.RED;

        Canvas.draw(sky);
        Canvas.draw(earth);
        Canvas.draw(wall);
        Canvas.draw(roof);
        Canvas.draw(window);
        Canvas.draw(sun);
    }
    */
}