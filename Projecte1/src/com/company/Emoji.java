package com.company;

import java.awt.*;

public class Emoji {
    Color faceColor;
    Color eyeCOlor;
    Color mouthColor;

    int size;
    int x, y;

    void draw(){
        Circle face = new Circle();
        face.color = faceColor;
        face.radius = size/2;
        face.positionX = x;
        face.positionY = y;

        Circle leftEye = new Circle();
        leftEye.color = eyeCOlor;
        leftEye.radius = size/8;
        leftEye.positionX = x+size/4-size/8;
        leftEye.positionY = y+size/3;

        Circle rightEye = new Circle();
        rightEye.color = eyeCOlor;
        rightEye.radius = size/8;
        rightEye.positionX = x+size/4*3-size/8;
        rightEye.positionY = y+size/3;

        Circle mouth = new Circle();
        mouth.color = mouthColor;
        mouth.radius = size/10;
        mouth.positionX = x+size/2-size/10;
        mouth.positionY = y+size/3*2;


        Canvas.draw(face);
        Canvas.draw(leftEye);
        Canvas.draw(rightEye);
        Canvas.draw(mouth);

    }
}