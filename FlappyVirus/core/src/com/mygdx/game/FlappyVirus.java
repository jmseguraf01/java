package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Arrays;
import java.util.Random;

public class FlappyVirus extends ApplicationAdapter {
	// Variables
	Random random;
	SpriteBatch batch;
	BitmapFont fuente;
	Texture virus, top, bottom, pantallaStart, pantallaGameOver;

	float virusX, virusY, virusVelocidadY;
	float gravedad = 0.4f;

	int numeroTubos = 2;
	float[] tuboX = new float[numeroTubos];
	float[] tuboY = new float[numeroTubos];

	int pantalla = 0; // Pantalla 0 = start , 1 = game, 2 = over
	float tiempo;
	
	@Override
	public void create () {
		random = new Random();
		batch = new SpriteBatch();
		fuente = new BitmapFont(Gdx.files.internal("press_start_2p.fnt"), Gdx.files.internal("press_start_2p_0.png"), false);

		// Cargo las imagenes
		virus = new Texture("virus.png");
		top = new Texture("top.png");
		bottom = new Texture("bot.png");
		pantallaStart = new Texture("start.png");
		pantallaGameOver = new Texture("over.png");

		restartPosition();
		tiempo = 0;
	}

	// Lo hace en bucle
	@Override
	public void render () {
		// Cuando es la pantalal del juego
		if (pantalla == 1) {

			// Tiempo
			tiempo += Gdx.graphics.getDeltaTime();

			Gdx.gl.glClearColor(1, 0.5f, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

			// Cuando pulso una tecla hago que suba el muñeco
			if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
				virusVelocidadY = 6.0f;
			}
			// Hago que vaya cayendo el virus
			virusY += virusVelocidadY;
			virusVelocidadY -= gravedad;
			batch.begin();

			// Cada vez el tubo va disminuyendo 5 la posicion x
			for (int i = 0; i < numeroTubos; i++) {
				tuboX[i] -= 5;
			}

			// Cuando los tubos se salen de la pantalla vuelven a salir
			for (int i = 0; i < numeroTubos; i++) {
				if (tuboX[i] < -80) {
					tuboX[i] = 640;
					tuboY[i] = -random.nextInt(290);
				}
			}

			// Compruebo si ha chocado
			for (int i = 0; i < numeroTubos; i++) {
				if (virusX < tuboX[i] + 80 && virusX + 64 > tuboX[i] && (virusY < tuboY[i] + 340 || virusY + 64 > tuboY[i] + 150 + 340)) {
					pantalla = 2;
				}
			}

			// Dibujo las imagenes
			batch.draw(virus, virusX, virusY);
			for (int i = 0; i < numeroTubos; i++) {
				batch.draw(bottom, tuboX[i], tuboY[i]);
				batch.draw(top, tuboX[i], tuboY[i] + 520);
			}

			batch.end();
		}

		// Pantalla de start
		else if (pantalla == 0) {
			// Si se pulsa una tecla, se inicia el juego
			if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
				pantalla = 1;
			}
			batch.begin();
			batch.draw(pantallaStart, 0, 0);
			batch.end();
		}

		// Pantalla de game over
		else if (pantalla == 2) {
			// Si se pulsa una tecla, se inicia el juego
			if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
				pantalla = 1;
				restartPosition();
			}

			batch.begin();
			batch.draw(pantallaGameOver, 0, 0);
			fuente.draw(batch, "Tiempo: " + String.format("%5.2f", tiempo), 195, 220);
			batch.end();
		}
	}

	// Funcion que reinicia la posicion de los elementos
	void restartPosition() {
		// Posicion del virus
		virusX = 64;
		virusY = 300;
		virusVelocidadY = 0;

		// Posicion de los tubos
		tuboX[0] = 600;
		tuboX[1] = tuboX[0] + 360;
		// Posicion de los tubos
		for (int i = 0; i < numeroTubos; i++) {
			tuboY[i] = -random.nextInt(290);
		}
		tiempo = 0;
	}

}
