package com.company;


import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        escogerDB();
    }

    static void escogerDB() {
        MenuDB menuDB = new MenuDB();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escojer Base de Datos");
        System.out.println("---------------------");
        System.out.println("1. Sqlite");
        System.out.println("2. Postgresql (se necesita un servidor)");
        System.out.println("");

        System.out.print("Escoge una opcion: ");
        int opcion = scanner.nextInt();

        // Sqlite
        if (opcion == 1) {
            menuDB.main("sqlite");
        }
        // Postgresql
        else if (opcion == 2) {
            menuDB.main("postgresql");
        }

    }

}
