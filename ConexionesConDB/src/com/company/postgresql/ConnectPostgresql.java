package com.company.postgresql;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectPostgresql {
    private Statement statement = null;
    public String url = "192.168.0.27:5432";
    public String database = "prueba_java";
    public String usuario;
    public String password;
    ResultSet resultadoConsulta;

    // Funcion que actualiza las credenciales
    public void guardarCredenciales(String usuario, String password) {
        this.usuario = usuario;
        this.password = password;
    }

    public Statement conectarBD() {
        try {
            statement =  DriverManager.getConnection("jdbc:postgresql://"+url+"/"+database+"", usuario, password).createStatement();
            return statement;
        } catch (SQLException e) {
            System.out.println("Error al conectar a la base de datos: " + e);
        }
        return statement;
    }

    public void insertarDatos() {
        try {
            statement.execute("insert into productos values('champu', 1.5)");
            System.out.println("Datos insertados.");
            System.out.println("");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void obtenerDatos() {
        try {
            resultadoConsulta = statement.executeQuery("select * from productos");
            while (resultadoConsulta.next()) {
                System.out.print(" | Producto: " + resultadoConsulta.getString("nombre"));
                System.out.print(" | Precio: " + resultadoConsulta.getInt("precio"));
                System.out.println("");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void obtenerDatosProducto(String producto) {
        try {
            resultadoConsulta = statement.executeQuery("select * from productos where nombre = '"+producto+"'");
            System.out.println("---------------------");
            while (resultadoConsulta.next()) {
                System.out.println("| Producto: " + resultadoConsulta.getString("nombre"));
                System.out.println("| Puntos: " + resultadoConsulta.getInt("precio"));
            }
            System.out.println("---------------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void obtenerDatosPrecio(int precio) {
        try {
            resultadoConsulta = statement.executeQuery("select * from productos where precio = '"+precio+"'");
            System.out.println("---------------------");
            while (resultadoConsulta.next()) {
                System.out.println("| Producto: " + resultadoConsulta.getString("nombre"));
                System.out.println("| Puntos: " + resultadoConsulta.getInt("precio"));
            }
            System.out.println("---------------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void desconectarDB() {
        try {
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
