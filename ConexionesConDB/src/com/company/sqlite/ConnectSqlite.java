package com.company.sqlite;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectSqlite {
    String databaseUrl = "database.db";
    Statement sentenciaSQL;
    ResultSet resultadoDeLaConsulta = null;

    public void conectar(){
        try {
            sentenciaSQL = DriverManager.getConnection("jdbc:sqlite:"+databaseUrl).createStatement();
            System.out.println("\nConexion establecida con la base de datos :)");
        }catch (SQLException ex) {
            System.err.println("No se ha podido conectar a la base de datos\n"+ex.getMessage());
        }
    }

    public void crearTabla() {
        try {
            sentenciaSQL.execute("CREATE TABLE prova(puntos integer, jugador text);");
            System.out.println("Tabla creada con éxito!");
        } catch (SQLException e) {
            System.out.println(e.getErrorCode());
        }
    }

    public void insertarDatos() {
        try {
            sentenciaSQL.execute("INSERT INTO prova VALUES(67, 'Juanmi');");
            sentenciaSQL.execute("INSERT INTO prova VALUES(23, 'Gerard');");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void obtenerDatos() {
        try {
            resultadoDeLaConsulta = sentenciaSQL.executeQuery("SELECT * FROM prova");
            System.out.println("******************************");
            while(resultadoDeLaConsulta.next()) {
                System.out.print("| Jugador: " + resultadoDeLaConsulta.getString("jugador"));
                System.out.println(" | Puntos: " + resultadoDeLaConsulta.getInt("puntos") + " |");
                System.out.println("");
            }
            System.out.println("******************************");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void obtenerDatosNombre(String nombre) {
        try {
            resultadoDeLaConsulta = sentenciaSQL.executeQuery("select * from prova where jugador = '"+nombre+"'");
            System.out.println("---------------------");
            while (resultadoDeLaConsulta.next()) {
                System.out.println("Jugador: " + resultadoDeLaConsulta.getString("jugador"));
                System.out.println("Puntos: " + resultadoDeLaConsulta.getInt("puntos"));
            }
            System.out.println("---------------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void obtenerDatosPuntos(int puntos) {
        try {
            resultadoDeLaConsulta = sentenciaSQL.executeQuery("select * from prova where puntos = '"+puntos+"'");
            System.out.println("---------------------");
            while (resultadoDeLaConsulta.next()) {
                System.out.println("Jugador: " + resultadoDeLaConsulta.getString("jugador"));
                System.out.println("Puntos: " + resultadoDeLaConsulta.getInt("puntos"));
            }
            System.out.println("---------------------");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void cerrarConexion(){
        try {
            sentenciaSQL.close();
            System.out.println("Conexión cerrada.");
        } catch (SQLException ex) {
            Logger.getLogger(ConnectSqlite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
