package com.company;

import com.company.postgresql.ConnectPostgresql;
import com.company.sqlite.ConnectSqlite;

import java.util.Scanner;

public class SeleccionarOpciones {
    Scanner scanner = new Scanner(System.in);
    String database;
    ConnectSqlite connectSqlite;
    ConnectPostgresql connectPostgresql;

    // Funcion para seleccionar las opciones
    public void seleccionarOpciones(boolean exit, ConnectSqlite connectDBSqlite, ConnectPostgresql connectPostgresql, String database) {
        // Actualizo las variables de la clase
        this.database = database;
        this.connectSqlite = connectDBSqlite;
        this.connectPostgresql = connectPostgresql;

        while (!exit) {
            Clear.clear();
            System.out.println("");
            System.out.println("Conexion a BD " + database + ".");
            System.out.println("--------------------------");
            System.out.println("1. Insertar datos");
            System.out.println("2. Obtener todos los datos");
            System.out.println("3. Consulta personalizada");
            System.out.println("0. Salir");

            System.out.print("Selecciona opcion: ");
            int opcion = scanner.nextInt();

            // Sqlite
            if (database.equals("sqlite")) {
                if (opcion == 1) {
                    connectDBSqlite.insertarDatos();
                } else if (opcion == 2) {
                    connectDBSqlite.obtenerDatos();
                } else if (opcion == 3) {
                    consultaPersonalizadaSqlite();
                } else if (opcion == 0) {
                    exit = true;
                }
            }

            // Postgresql
            else if (database.equals("postgresql")) {
                if (opcion == 1) {
                    connectPostgresql.insertarDatos();
                } else if (opcion == 2) {
                    connectPostgresql.obtenerDatos();
                } else if (opcion == 3) {
                    consultaPersonalizadaPostgresql();
                } else if (opcion == 0) {
                    exit = true;
                }
            }

            if (!exit) {
                System.out.print("Escribe cualquier palabra para continuar . . .");
                scanner.next();
                Clear.clear();
            }

        }
    }

    // Cuando se escoje una consulta personalizada
    private void consultaPersonalizadaSqlite() {
        System.out.println("");
        System.out.println("Consulta personalizada");
        System.out.println("----------------------");
        System.out.println("1. Consultar por nombre");
        System.out.println("2. Consultar por puntos");
        System.out.println("0. Salir");
        System.out.println("");
        System.out.print("Selecciona opcion: ");
        int opcion = scanner.nextInt();
        System.out.println("");

        if (opcion == 1) {
            scanner.nextLine();
            System.out.print("Escribe el nombre de la persona que quieres consultar: ");
            connectSqlite.obtenerDatosNombre(scanner.nextLine());
        }

        else if (opcion == 2) {
            System.out.print("Escribe los puntos que quieres consultar: ");
            connectSqlite.obtenerDatosPuntos(scanner.nextInt());
        }

    }

    // Consulta personalizada para base de datos postgres
    private void consultaPersonalizadaPostgresql() {
        System.out.println("");
        System.out.println("Consulta personalizada");
        System.out.println("----------------------");
        System.out.println("1. Consultar por nombre ");
        System.out.println("2. Consultar por precio");
        System.out.println("0. Salir");
        System.out.println("");
        System.out.print("Selecciona la opcion: ");
        int opcion = scanner.nextInt();
        System.out.println("");

        if (opcion == 1) {
            scanner.nextLine(); // Intro del nextInt
            System.out.print("Escribe el nombre del producto que deseas buscar: ");
            connectPostgresql.obtenerDatosProducto(scanner.nextLine());
        } else if (opcion == 2) {
            System.out.print("Escribe el precio por el que deseas buscar: ");
            connectPostgresql.obtenerDatosPrecio(scanner.nextInt());
        }

    }

}
