package com.company;

import com.company.postgresql.ConnectPostgresql;
import com.company.sqlite.ConnectSqlite;

import java.sql.Statement;
import java.util.Scanner;

public class MenuDB {
    static Scanner scanner = new Scanner(System.in);
    boolean exit = false;

    SeleccionarOpciones seleccionarOpciones = new SeleccionarOpciones();
    // Creo el objeto de las clases de la BD
    ConnectSqlite connectDBSqlite = new ConnectSqlite();
    ConnectPostgresql connectPostgresql = new ConnectPostgresql();

    // Menu
    public void main(String database) {

        // Sqlite
        if (database.equals("sqlite")) {
            // Creo los objetos de las clases de sqlite
            connectDBSqlite.conectar();
            seleccionarOpciones.seleccionarOpciones(exit, connectDBSqlite, connectPostgresql, database);
            connectDBSqlite.cerrarConexion();

        }

        // Postgresql
        else if (database.equals("postgresql")) {
            // Pido las credenciales
            credencialesPostgresql();
            // Creo los objetos de las clases de postgres
            Statement statement = connectPostgresql.conectarBD();
            // Si la conexión ha sido establecida
            if (statement != null) {
                seleccionarOpciones.seleccionarOpciones(exit, connectDBSqlite, connectPostgresql, database);
                connectPostgresql.desconectarDB();
            }
        }

    }

    // Funcion que solicita las credenciales de postgres y las guarda en la conexion con la BD
    private void credencialesPostgresql() {
        System.out.println("");
        System.out.println("Credenciales de postgresql");
        System.out.println("---------------------------");
        System.out.print("Introduce el nombre de usuario: ");
        String usuario = scanner.nextLine();
        System.out.print("Introduce la contraseña: ");
        String password = scanner.nextLine();
        // Le paso las credenciales para que las guarde
        connectPostgresql.guardarCredenciales(usuario, password);
    }


}
