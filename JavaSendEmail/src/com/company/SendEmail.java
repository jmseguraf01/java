package com.company;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Properties;

public class SendEmail {
    public static void enviarConGMail(String nombreUsuario, String password, String destinatario, String asunto, String texto) {

        Properties props = System.getProperties();
        // El servidor SMTP de Google
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        // Cuenta y password
        props.put("mail.smtp.user", nombreUsuario);
        props.put("mail.smtp.clave", password);
        props.put("mail.smtp.auth", "true");
        // Conectarme de forma segura
        props.put("mail.smtp.starttls.enable", "true");
        // Puerto seguro
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(nombreUsuario));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));
            message.setSubject(asunto);
            message.setText(texto,"ISO-8859-1","html");

            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", nombreUsuario, password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
            System.out.println("Se ha enviado el correo a " + destinatario);
        }
        catch (javax.mail.AuthenticationFailedException a) {
            System.out.println("");
            System.out.println("Error al conectar a la cuenta " + nombreUsuario + ".");
            System.out.println("Recuerda que en la cuenta debes tener activado el acceso a aplicaciones menos seguras.");
            System.out.println("Enlace: https://myaccount.google.com/u/1/security");
        }
        // Si se produce un error
        catch (MessagingException me) {
            me.printStackTrace();
        }
    }
}
