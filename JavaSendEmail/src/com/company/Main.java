package com.company;

import java.io.Console;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Menu
        System.out.println("----------------------------");
        System.out.println("  CORREO NAVIDEÑO CON JAVA  ");
        System.out.println("----------------------------");
        System.out.println("");
        System.out.println("Autentificación: ");
        System.out.println("----------------");
        // Variables
        String nombreUsuario;
        String password;
        String destinatario;
        String asunto = "Felices fiestas desde Java.";
        String texto = "<h1 style=\"color:blue; font-style:oblique; font-size:25px; font-family:'Pacifico', cursive;\">Feliz navidad y próspero año 2020!</h1>" + "<img src=\"https://thumbs.gfycat.com/LargeWelldocumentedHawaiianmonkseal-size_restricted.gif\" width=\"450\">" + "<p style=\"color:blue; font-style:oblique; font-size:15px; font-family:'Pacifico', cursive\">Este es un correo enviado desde Java.</p>" + "<p style=\"color:green;font-style:oblique;font-size:20px;\">Created by: Juan Miguel.</p>" + "<p><a href=\"https://gitlab.com/jmseguraf01/java/tree/master/JavaSendEmail\">Enlace al código del proyecto Java</a></p>";
        // Preguntas
        System.out.print("Escribe la direccion de correo desde la cual quieres enviar los mensajes: ");
        nombreUsuario = scanner.nextLine();
        System.out.print("Escribe la contraseña de la cuenta " + nombreUsuario + ": ");
        password = scanner.nextLine();
        System.out.println("-----------------");
        System.out.print("Escribe la direccion de correo de destino: ");
        destinatario = scanner.nextLine();


        // Envio la informacion a la clase que envia el correo
        SendEmail.enviarConGMail(nombreUsuario, password, destinatario, asunto, texto);
    }
}
