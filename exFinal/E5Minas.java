package com.company;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int x = scanner.nextInt();
        int y = scanner.nextInt();

        int[][] tablero = new int[x][y];
        // Relleno el tablero
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                tablero[i][j] = scanner.nextInt();
            }
        }


        // Compruebo
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                int mina = 0;
                // Si tiene una mina
                if (tablero[i][j] == -1) {
                    System.out.print("*" + " ");
                }

                // Si no tiene mina
                else {
                    // Si tiene una mina abajo
                    if (i != (x-1)) {
                        if (tablero[i + 1][j] == -1) {
                            mina++;
                        }
                    }

                    // Si tiene una mina arriba
                    if (i != 0) {
                        if (tablero[i - 1][j] == -1) {
                            mina++;
                        }
                    }

                    // Tiene una mina a la derecha
                    if (j != (y-1)) {
                        if (tablero[i][j + 1] == -1 ) {
                            mina++;
                        }
                    }
                    // Tiene una mina a la izquierda
                    if (j != 0 ) {
                        if (tablero[i][j - 1] == -1) {
                            mina++;
                        }
                    }

                    // Tiene una mina a la derecha y arriba
                    if (i != 0 && j != (y-1) ) {
                        if (tablero[i-1][j+1] == -1) {
                            mina++;
                        }
                    }

                    // Tiene una mina a la derecha y abajo
                    if (i != (x-1) && j != (y-1) ) {
                        if (tablero[i+1][j+1] == -1) {
                            mina++;
                        }
                    }

                    // Tiene una mina izquierda arriba
                    if (i != 0 && j !=0) {
                        if (tablero[i - 1][j-1] == -1) {
                            mina++;
                        }
                    }

                    // Tiene una mina izquierda abajo
                    if (i != (x-1) && j != 0) {
                        if (tablero[i + 1][j-1] == -1) {
                            mina++;
                        }
                    }

                    System.out.print(mina + " ");
                }
            }
            System.out.println("");
        }




    }
}
