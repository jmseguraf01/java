package com.company;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int filas = scanner.nextInt();
        int columnas = scanner.nextInt();

        // Informacion del jugador 1
        int[][] matrizJugador1 = new int[filas][columnas];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matrizJugador1[i][j] = scanner.nextInt();
            }
        }

        // Informacion del jugador 2
        int[][] matrizJugador2 = new int[filas][columnas];
        for (int i = 0; i < filas; i++) {
            for (int j = 0; j < columnas; j++) {
                matrizJugador2[i][j] = scanner.nextInt();
            }
        }
        
        // Disparos
        int numDisparos = scanner.nextInt();
        // Fila y columna del disparo
        int filaDisparo = 0;
        int columnaDisparo = 0;

        int puntosJugador1 = 0;
        int puntosJugador2 = 0;

        // Por cada 2 jugadores
        for (int i = 0; i < 2; i++) {
            // Por cada numero de disparos
            for (int j = 0; j < numDisparos; j++) {
                filaDisparo = scanner.nextInt();
                columnaDisparo = scanner.nextInt();
                // Jugador 1
                if (i == 0) {
                    // Si hay barco en la posicion indicada, sumo 1 punto al jugador 1
                    if (matrizJugador1[filaDisparo-1][columnaDisparo-1] == 1) {
                        puntosJugador1++;
                    }
                }
                // Jugador 2
                else if (i == 1) {
                    // Si hay barco en la posicion indicada, sumo 1 punto al jugador 2
                    if (matrizJugador2[filaDisparo-1][columnaDisparo-1] == 1) {
                        puntosJugador2++;
                    }
                }
            }
        }

        if (puntosJugador1 > puntosJugador2) {
            System.out.println("player1");
        }
        else if (puntosJugador2 > puntosJugador1) {
            System.out.println("player2");
        }
        else if (puntosJugador1 == puntosJugador2) {
            System.out.println("tie");
        }

    }
}
