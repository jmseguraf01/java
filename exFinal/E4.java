package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class E4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Arrays
        int[] productos = new int[10];
        int[] precios = new int[10];
        int[] stock = new int[10];

        // Guardo en los array las caracteristicas de los productos
        for (int i = 0; i < 10; i++) {
            productos[i] = scanner.nextInt();
            precios[i] = scanner.nextInt();
            stock[i] = scanner.nextInt();
        }


        // Numero de pedidos
        int numero = scanner.nextInt();
        int[] factura = new int[numero];

        // Los pedidos
        for (int i = 0; i < numero; i++) {
            // Array de la factura total

            // Numero de productos en esta factura
            int numProductos = scanner.nextInt();
            // El id del producto y la cantidad que se solicita
            int idproducto = 0;
            int cantidadProducto = 0;

            for (int j = 0; j < numProductos; j++) {
                idproducto = scanner.nextInt();
                cantidadProducto = scanner.nextInt();

                // Busco en los productos, el precio de este producto y añado en la factura
                for (int k = 0; k < 10; k++) {
                    if (productos[k] == idproducto) {
                        // En el caso de que no hayan suficientes unidades en stock, vendo las maximas posibles
                        if (cantidadProducto > stock[k]) {
                            cantidadProducto = stock[k];
                        }
                        else {
                            // Actualizo el stock
                            stock[k] = stock[k] - cantidadProducto;
                        }
                        // Añado el precio a la factura
                        factura[i] = factura[i] + (cantidadProducto * precios[k]);

                    }
                }

            }
        }

        // Imprimo las facturas
        for (int i = 0; i < numero; i++) {
            System.out.println("Total factura) " + factura[i]);
        }



    }
}


/*
1 200 5000
2 10 5000
3 200 5000
4 100 5000
5 1000 5000
6 2000 5000
7 3000 5000
8 10 5000
9 100 5000
10 2000 5000
3
2 9 100 32 5000
1 1 1000
3 2 6000 1 5000 99 1
*/
