package com.company;

import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // Numero de discos
        int numDiscos = scanner.nextInt();
        int repartidos = scanner.nextInt();
        int bitsTotal = scanner.nextInt();

        // Los 3 discos, y en cada uno se guarda el total de bits dividido en los discos que hay que guardarlos
        // sin contar el de pariedad que es el ultimo
        int[][] discos = new int[numDiscos-1][bitsTotal/(numDiscos-1)];

        int leidos = 0;
        int vecesPasadas = 0;
        // Siempre que no haya pasado todas las veces
        while (leidos < bitsTotal) {
            vecesPasadas++;
            // Por cada disco
            for (int i = 0; i < numDiscos - 1; i++) {
                int a = 0;
                for (int j = leidos; j < bitsTotal; j++) {
                    // Siempre que este dentro del rango de bits que se tienen que repartir
                    if (a < repartidos) {
                        // Creo una variable temporal
                        int jPuntual = j;
                        // Si ya ha pasado anteriormente
                        if (vecesPasadas > 1) {
                            // La variable jPuntual, es por donde se ha quedado la ultima vez
                            jPuntual = (leidos / (numDiscos - 1));
                        }
                        // Guardo
                        discos[i][jPuntual] = scanner.nextInt();
                    }
                    // Si ha pasado de el numero de bits repartidos, hay que salir
                    else {
                        break;
                    }
                    a++;
                    leidos++;
                }
            }
        }

        for (int i = 0; i < numDiscos-1; i++) {
            System.out.print("Disco" + i + " ");
            for (int j = 0; j < bitsTotal / (numDiscos - 1); j++) {
                System.out.print(discos[i][j]);
            }
            System.out.println("");
        }
        
    }
}