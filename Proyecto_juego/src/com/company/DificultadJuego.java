package com.company;

import java.util.Scanner;

public class DificultadJuego {
    static int dificultadJuego = 0;
    static int segundosMaximos = 0;
    // Funcion para escojer dificultad
    static void escogerDificultad() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("------------------------");
        System.out.println("  D I F I C U L T A D ");
        System.out.println("------------------------");
        System.out.println("\t 1. Easy mode -> 30 segundos. ");
        System.out.println("\t 2. Normal mode -> 20 segundos. ");
        System.out.println("\t 3. Hard mode -> 10 segundos.");
        // Escoje dificultad del juego
        System.out.print("Escoge el numero de la dificultad a la que desees jugar: ");
        dificultadJuego = scanner.nextInt();
        // Calculamos el tiempo maximo que puede tardar el usuario
        // Dificultad facil
        if (dificultadJuego == 1) {
            segundosMaximos = 30;
        }
        // Dificultad media
        else if (dificultadJuego == 2) {
            segundosMaximos = 20;
        }
        // Dificultad dificil
        else if (dificultadJuego == 3) {
            segundosMaximos = 10;
        }
    }
}
