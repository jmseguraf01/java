package com.company;

public class Mapa {
    // Funcion que pinta el mapa por la pantalla
    static void printmap() {
        Main.clear();
        System.out.println("¡¡¡ Recuerda que solo dispones de "+ DificultadJuego.segundosMaximos+ " segundos !!!");
        System.out.println("El mapa es el siguiente: ");
        System.out.println("--------------------------");
        // Llamo a la funcion que me devuelve el mapa creado
        String[] mapa = createmap();
        Juego juego = new Juego();
        juego.game(mapa);
    }

    // Funcion que crea el mapa y lo devuelve creado
    static String[] createmap() {
        // Convoco la clase
        Objectesmap objectesmap = new Objectesmap();
        // Creo un mapa de 20 caracteres
        for (int i = 0; i < objectesmap.longitudJuego; i++) {
            int numRandom = (int) (Math.random()*i) * 2 + 1;
            // Creo un algoritmo para escoger que tipo de caracter poner
            if (numRandom > i) {
                System.out.print(objectesmap.salto + " ");
                objectesmap.mapaJuego[i] = objectesmap.salto;
            } else if (numRandom < i) {
                System.out.print(objectesmap.suelo + " ");
                objectesmap.mapaJuego[i] = objectesmap.suelo;
            } else {
                System.out.print(objectesmap.obstaculo + " ");
                objectesmap.mapaJuego[i] = objectesmap.obstaculo;
            }
        }
        System.out.println("");
        return objectesmap.mapaJuego;
    }
}
