package com.company;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Juego {
    void game(String[] mapa) {
        Scanner scanner = new Scanner(System.in);
        // Arrays
        String[] solucionFinal = new String[Objectesmap.longitudJuego];
        String[] respuestaJugador = new String[Objectesmap.longitudJuego];

        // Contador de errores
        int countError = 0;

        // Segundos al empezar el juego
        int segundosEmpezar = LocalTime.now().getSecond();

        // Introduce el jugador los caracteres
        for (int i = 0; i < Objectesmap.longitudJuego; i++) {
            respuestaJugador[i] = scanner.next();
        }

        int segundosTardados = LocalTime.now().getSecond() - segundosEmpezar;

        // Miramos a ver si ha tardado mas del tiempo permitido
        if (segundosTardados > DificultadJuego.segundosMaximos) {
            System.out.println("Has tardado mas de " + DificultadJuego.segundosMaximos + " segundos! Perdiste!");
            // Volver a jugar
            playAgain();

        } else {
            System.out.println("Has tardado menos de " + DificultadJuego.segundosMaximos + " segundos! Pasamos a la correccion...");
            if (segundosTardados < 0 ) {
                segundosTardados =  60 - Math.abs(segundosTardados);
            }
            System.out.println("Has tardado concretamente " + segundosTardados + " segundos");
            sleep(2000);
        }

        // Aqui comprobamos el mapa y lo que ha introducido el jugador
        for (int i = 0; i < Objectesmap.longitudJuego; i++) {
            // Clear
            Main.clear();
            // Cuando sea un salto y no salte el jugador
            if (mapa[i].equals(Objectesmap.salto) && !respuestaJugador[i].equals(CaracterJugador.salto)) {
                countError++;
                solucionFinal[i] = "\u001B[31m" + mapa[i] + " ";
                // solucionFinal[i] = mapa[i];
                ImprimirFor(i, "\u001B[31m x ", solucionFinal, mapa);
            }

            // Cuando sea un obstaculo y el jugador no salte
            else if (mapa[i].equals(Objectesmap.obstaculo) && !respuestaJugador[i].equals(CaracterJugador.salto)) {
                // Cuando sea un obstaculo y el jugador no agache
                if (mapa[i].equals(Objectesmap.obstaculo) && !respuestaJugador[i].equals(CaracterJugador.agacharse)) {
                    countError++;
                    solucionFinal[i] = "\u001B[31m" + mapa[i] + " ";
                    ImprimirFor(i, "\u001B[31m x ", solucionFinal, mapa);
                }
                // Cuando si que se agacha
                else {
                    solucionFinal[i] = "\u001B[32m" + mapa[i] + " ";
                    ImprimirFor(i, "\u001B[32m x ", solucionFinal, mapa);
                }
            }

            // Cuando sea suelo y el jugador no ande
            else if (mapa[i].equals(Objectesmap.suelo) && !respuestaJugador[i].equals(CaracterJugador.andar)) {
                countError++;
                solucionFinal[i] = "\u001B[31m" + mapa[i] + " ";
                ImprimirFor(i, "\u001B[31m x ", solucionFinal, mapa);
            }

            // Si el jugador ha indicado la orden correcta
            else {
                solucionFinal[i] = "\u001B[32m" + mapa[i] + " ";
                ImprimirFor(i, "\u001B[32m x ", solucionFinal, mapa);
            }
            sleep(500);
        }
        // Imprime error en plural o en singular si el numero de errores es mayor a 1
        System.out.println("");
        System.out.println("");
        if (countError > 1){
            System.out.println("\u001B[37mHas tenido " + "\u001B[31m" + countError + " errores.");
        } else if (countError == 1){
            System.out.println("\u001B[37mHas tenido " + "\u001B[31m" + countError + " error.");
        } else if(countError == 0){
            System.out.println("\u001B[32m¡Enhorabuena no has tenido ningun error!");
        }
        // Volver a jugar
        playAgain();
    }

    // Funcion que imprime todoo lo de antes de X y lo de despues
    void ImprimirFor(int i, String imprimir, String[] solucionFinal, String[] mapa) {
        for (int j = 0; j < Objectesmap.longitudJuego; j++) {
            // Cuando esta en la misma posicion imprimimos x
            if (j == i) {
                System.out.print(imprimir);
            }
            // Cuando estamos en una posicion anterior a X
            else if (j < i){
                System.out.print(solucionFinal[j]);
            }
            // Cuando estamos en una posicion mayor a X
            else if (j > i) {
                System.out.print("\u001B[37m" + mapa[j] + " ");
            }
        }
    }

    // Funcion para esperar un tiempo
    static void sleep(int milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // Funcion para volver a jugar
    void playAgain() {
        Scanner scanner = new Scanner(System.in);
        // Volver a jugar
        System.out.print("\u001B[37m¿Quieres volver a jugar? (S/N): ");
        String volverJugar = scanner.next();
        if (volverJugar.equals("S") || volverJugar.equals("s")){
            DificultadJuego.escogerDificultad();
            Mapa.printmap();
        } else {
            System.out.println("¡Gracias por jugar! Vuelve pronto...");
            System.exit(0);
        }
    }

}



/*
s w w w s w s s w w w w w w w w w w s s s w s w w w w w w w
 O
/|\
w s

x   x     x   x   x
| - | x x | x | x | | _ | _ | - | _ _ _ _ _ _ _ | _ | _ | _
  x

| X | _ _ | _ | _ | | _ | _ | - | _ _ _ _ _ _ _ | _ | _ | _

| - x | | _ | | | _ _ | | | _ _ _ _ _ _ _ _ | | | | _ | _ |


* */
