package com.company;

class CaracterJugador {
    // La ponemos static porque son variables que unicamente las convocamos pero no le cambiamos su valor
    static String salto = "w";
    static String andar = "d";
    static String agacharse = "s";
}