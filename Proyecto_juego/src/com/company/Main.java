package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("-------------------------------------------");
        System.out.println("  B I E N V E N I D O    A L    J U E G O");
        System.out.println("-------------------------------------------");
        System.out.println("Instruciones: ");
        System.out.println("\t - El juego te mostrará un mapa, el cual estará compuesto de suelo, obstaculos y agujeros que tendrás que saltar.");
        System.out.println("\t - Una vez mostrado el mapa deberás introducir, siguiendo la leyenda, los pasos que debe hacer el jugador en cada posicion.");
        System.out.println("\t - Leyenda del mapa en caracteres: Suelo -> _    / Salto -> |   / Obstaculo -> - " );
        System.out.println("\t - Lo que tu deberás introducir será: Para avanzar cuando haya suelo -> d   / Para saltar cuando haya salto o obstaculo -> w   / Para agacharte cuando haya pbstaculo -> s" );
        System.out.print("\nEmpezar el juego? (S/N): ");
        scanner.next();
        String empezarJuego = "s";
        // Empezar el juego
        if (empezarJuego.equals("S") || empezarJuego.equals("s")) {
            DificultadJuego.escogerDificultad();
            Juego game = new Juego();
            Mapa.printmap();
        }


    }

    // Funcion clear de pantalla
    static void clear() {
        for (int i = 0; i < 25; i++) {
            System.out.println("");
        }
    }
}
