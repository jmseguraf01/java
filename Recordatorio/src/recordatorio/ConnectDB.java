package recordatorio;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectDB {
    
    private String databaseUrl = "data/database.db";
    private Statement sentenciaSQL;
    private ResultSet resultadoDeLaConsulta = null;

    // Conecta a la base de datos
    void connect() {
        try {
            sentenciaSQL = DriverManager.getConnection("jdbc:sqlite:"+databaseUrl).createStatement();
            System.out.println("Conectado a la Bd.");
        } catch (SQLException ex) {
            System.out.println("Error al conectar a la BD + " + ex.getMessage());
        }
    }
   
    // Obtengo los usuarios y los passwords
    ResultSet getData() {
        try {
            resultadoDeLaConsulta = sentenciaSQL.executeQuery("select * from user");
        } catch (SQLException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultadoDeLaConsulta;
    }
    
            
    // Close conexion
    void close() {
        try {
            sentenciaSQL.close();
            System.out.println("Conexion con DB cerrada.");
        } catch (SQLException ex) {
            Logger.getLogger(ConnectDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
