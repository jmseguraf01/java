package piedrapapeltijera_grafico;

import java.awt.Dimension;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;

/**
 *
 * @author jusef
 */
public class Interface_ganador extends javax.swing.JFrame {

    public Interface_ganador() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButtonVolverAjugar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ganador!");
        setResizable(false);
        getContentPane().setLayout(null);

        jLabel1.setBackground(new java.awt.Color(255, 102, 102));
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/ganado_letra.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(300, 190, 350, 104);

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/ganado.png"))); // NOI18N
        getContentPane().add(jLabel2);
        jLabel2.setBounds(0, 0, 650, 292);

        jButtonVolverAjugar.setBackground(new java.awt.Color(51, 51, 51));
        jButtonVolverAjugar.setFont(new java.awt.Font("Dialog", 3, 24)); // NOI18N
        jButtonVolverAjugar.setForeground(new java.awt.Color(204, 204, 0));
        jButtonVolverAjugar.setText("Volver a jugar");
        jButtonVolverAjugar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonVolverAjugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVolverAjugarActionPerformed(evt);
            }
        });
        getContentPane().add(jButtonVolverAjugar);
        jButtonVolverAjugar.setBounds(0, 290, 650, 80);

        setSize(new java.awt.Dimension(655, 393));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
    // Funcion inicial
    public static void menu() {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
        * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
        */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interface_ganador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interface_ganador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interface_ganador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interface_ganador.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interface_ganador().setVisible(true);
            }
        });
    }
    
    // Click en el boton de volver a jugar
    private void jButtonVolverAjugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVolverAjugarActionPerformed
        Interface.jugarDeNuevo();
        Interface_ganador.this.setVisible(false);
    }//GEN-LAST:event_jButtonVolverAjugarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonVolverAjugar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    // End of variables declaration//GEN-END:variables
}
