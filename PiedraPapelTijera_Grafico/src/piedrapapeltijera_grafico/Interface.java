package piedrapapeltijera_grafico;

/**
 *
 * @author juanmi
 */
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import javax.swing.JPanel;

public class Interface extends javax.swing.JFrame {
    public Interface() {
        initComponents();
        // Para que no se pueda hacer grande el jframe
        this.setResizable(false);
    }
    
    
    
    public static void main(String[] args) {
        menu();
    }
    
    public static void menu() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Interface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Interface().setVisible(true);
            }
        });
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButtonTijera = new javax.swing.JButton();
        jButtonPiedra = new javax.swing.JButton();
        jButtonPapel = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        label_puntos_jugador = new javax.swing.JLabel();
        label_result_jugador = new javax.swing.JLabel();
        label_check_jugador = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButtonPapel2 = new javax.swing.JButton();
        jButtonPiedra2 = new javax.swing.JButton();
        jButtonTijera2 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        label_puntos_maquina = new javax.swing.JLabel();
        label_result_maquina = new javax.swing.JLabel();
        label_check_maquina = new javax.swing.JLabel();
        Label_vs = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        MenuItemAcercaDe = new javax.swing.JMenuItem();
        MenuItemLicencia = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        MenuItemCerrar = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Juego de Piedra / Papel / Tijera");
        setMaximumSize(new java.awt.Dimension(0, 0));

        jPanel1.setBackground(new java.awt.Color(50, 125, 192));

        jButtonTijera.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/tijera.png"))); // NOI18N
        jButtonTijera.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonTijera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonTijeraActionPerformed(evt);
            }
        });

        jButtonPiedra.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/piedra.png"))); // NOI18N
        jButtonPiedra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPiedraActionPerformed(evt);
            }
        });

        jButtonPapel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/papel.png"))); // NOI18N
        jButtonPapel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButtonPapel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPapelActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Segoe Script", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(244, 177, 28));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Piedra");

        jLabel4.setFont(new java.awt.Font("Times New Roman", 2, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 102, 0));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Jugador");

        label_puntos_jugador.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_puntos_jugador.setText("Puntos: 0");

        jLabel6.setFont(new java.awt.Font("Tahoma", 2, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("V1.0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonPiedra)
                    .addComponent(jButtonTijera)
                    .addComponent(jButtonPapel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 128, Short.MAX_VALUE)
                .addComponent(label_result_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(190, 190, 190)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(label_puntos_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(383, Short.MAX_VALUE)
                    .addComponent(label_check_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(40, 40, 40)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(label_puntos_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jButtonTijera)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonPapel))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(163, 163, 163)
                        .addComponent(label_result_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jButtonPiedra)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(390, Short.MAX_VALUE)
                    .addComponent(label_check_jugador, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(49, 49, 49)))
        );

        jPanel3.setBackground(new java.awt.Color(244, 177, 28));

        jButtonPapel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/papel.png"))); // NOI18N
        jButtonPapel2.setEnabled(false);

        jButtonPiedra2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/piedra.png"))); // NOI18N
        jButtonPiedra2.setEnabled(false);

        jButtonTijera2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/tijera.png"))); // NOI18N
        jButtonTijera2.setEnabled(false);

        jLabel3.setFont(new java.awt.Font("Segoe Script", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(94, 175, 227));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Tijera");

        jLabel5.setFont(new java.awt.Font("Times New Roman", 2, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(153, 0, 0));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Maquina");

        label_puntos_maquina.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        label_puntos_maquina.setText("Puntos: 0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(label_result_maquina, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addComponent(label_check_maquina, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(39, 39, Short.MAX_VALUE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jButtonPiedra2)
                                    .addComponent(jButtonTijera2)
                                    .addComponent(jButtonPapel2))
                                .addGap(69, 69, 69))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(label_puntos_maquina, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(83, 83, 83)
                                .addComponent(jLabel5)))))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(label_puntos_maquina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(32, 32, 32)
                .addComponent(jButtonTijera2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(label_result_maquina, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButtonPapel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jButtonPiedra2)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(label_check_maquina, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47))))
        );

        Label_vs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/imagen_vs.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Segoe Script", 1, 36)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Papel");

        jMenuBar1.setBorder(null);
        jMenuBar1.setForeground(new java.awt.Color(255, 255, 255));
        jMenuBar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jMenu1.setBackground(new java.awt.Color(204, 204, 204));
        jMenu1.setText("Opciones");
        jMenu1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));

        MenuItemAcercaDe.setText("Acerca de");
        MenuItemAcercaDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItemAcercaDeActionPerformed(evt);
            }
        });
        jMenu1.add(MenuItemAcercaDe);

        MenuItemLicencia.setText("Ver licencia");
        MenuItemLicencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItemLicenciaActionPerformed(evt);
            }
        });
        jMenu1.add(MenuItemLicencia);
        jMenu1.add(jSeparator1);
        jMenu1.add(jSeparator2);

        MenuItemCerrar.setText("Cerrar");
        MenuItemCerrar.setToolTipText("");
        MenuItemCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuItemCerrarActionPerformed(evt);
            }
        });
        jMenu1.add(MenuItemCerrar);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(Label_vs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(77, 77, 77)
                .addComponent(Label_vs, javax.swing.GroupLayout.PREFERRED_SIZE, 464, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 13, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    // Click boton papel
    private void jButtonPapelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPapelActionPerformed
        opcionEscogida = "papel";
        label_result_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/papel.png")));
        empezar(opcionEscogida, opcionEscogidaJugador2);
    }//GEN-LAST:event_jButtonPapelActionPerformed

    // Click boton piedra
    private void jButtonPiedraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPiedraActionPerformed
        opcionEscogida = "piedra";
        // Actualizo la imagen de la label
        label_result_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/piedra.png")));
        empezar(opcionEscogida, opcionEscogidaJugador2);
    }//GEN-LAST:event_jButtonPiedraActionPerformed

    // Click boton tijera
    private void jButtonTijeraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonTijeraActionPerformed
        opcionEscogida = "tijera";
        label_result_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/tijera.png")));
        empezar(opcionEscogida, opcionEscogidaJugador2);
    }//GEN-LAST:event_jButtonTijeraActionPerformed

    // Click en el menu item de cerrar
    private void MenuItemCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItemCerrarActionPerformed
        System.exit(0);
    }//GEN-LAST:event_MenuItemCerrarActionPerformed

    // Click en el menu item acerca de
    private void MenuItemAcercaDeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItemAcercaDeActionPerformed
        Acerca_de.menu();
    }//GEN-LAST:event_MenuItemAcercaDeActionPerformed

    // Click en el menu item ver licencia
    private void MenuItemLicenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuItemLicenciaActionPerformed
        Licencia.menu();
    }//GEN-LAST:event_MenuItemLicenciaActionPerformed

    // Variables
    String opcionEscogida = "";
    String opcionEscogidaJugador2 = "";
    int puntos = 0;
    int puntosMaquina = 0;

    
    // Funcion para empezar el juego
    void empezar(String opcionEscogida, String opcionEscogidaJugador2) {
        // Variables
        boolean gana = false;
        boolean empate = false;
        int opcionJugador2 = 0;

        // Hago que la maquina escoja una opcion de forma aleatoria
        opcionJugador2 = (int) (Math.random() * 3 + 1);

        // Compruebo jugador 2
        // Piedra
        if (opcionJugador2 == 1) {
            opcionEscogidaJugador2 = "piedra";
            // Acualizo la imagen
            label_result_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/piedra.png")));
        } 
        
        // Papel
        else if (opcionJugador2 == 2) {
            opcionEscogidaJugador2 = "papel";
            label_result_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/papel.png")));
        } 
        
        // Tijera
        else if (opcionJugador2 == 3) {
            opcionEscogidaJugador2 = "tijera";
            label_result_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/tijera.png")));
        }
        
        // Resultados
        // ----------
        // Piedra
        // Pierde
        if (opcionEscogida.equals("piedra") && opcionEscogidaJugador2.equals("papel") ) {
            pierde();
        }
        // Gana
        else if (opcionEscogida.equals("piedra") && opcionEscogidaJugador2.equals("tijera") ) {
            gana();
        }
        // Empate
        else if (opcionEscogida.equals("piedra") && opcionEscogidaJugador2.equals("piedra") ) {
            empate();
        }
        
        // Papel
        // Gana
        else if (opcionEscogida.equals("papel") && opcionEscogidaJugador2.equals("piedra") ) {
            gana();
        }
        // Pierde
        else if (opcionEscogida.equals("papel") && opcionEscogidaJugador2.equals("tijera") ) {
            pierde();
        }
        // Empate
        else if (opcionEscogida.equals("papel") && opcionEscogidaJugador2.equals("papel") ) {
            empate();
        }
        
        // Tijera
        // Pierde
        else if (opcionEscogida.equals("tijera") && opcionEscogidaJugador2.equals("piedra") ) {
            pierde();
        }
        // Gana
        else if (opcionEscogida.equals("tijera") && opcionEscogidaJugador2.equals("papel") ) {
            gana();
        }
        // Empate
        else if (opcionEscogida.equals("tijera") && opcionEscogidaJugador2.equals("tijera") ) {
            empate();
        }
    }

    // Suma un punto al jugador
    void gana() {
        puntos++;
        // Actualizo puntos y imagen del check
        label_puntos_jugador.setText("Puntos: " + puntos);
        label_check_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/check.png")));
        // Imagen x.png al otro
        label_check_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/x.png")));
        // Reseteo el color de los puntos de la maquina
        label_puntos_maquina.setForeground(new java.awt.Color(0, 0, 0));
        label_puntos_jugador.setForeground(new java.awt.Color(1, 88, 0));
        // Cuando ha ganado llamo a la otra interfaz
        if (puntos == 3) {
            Interface_ganador.menu();
            Interface.this.setVisible(false);
        }

    }
    
    // Suma un punto a la maquina
    void pierde() {
        puntosMaquina++;
        // Actualizo puntos y imagen del check
        label_puntos_maquina.setText("Puntos: " + puntosMaquina);
        label_check_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/check.png")));
        // Imagen x.png al otro
        label_check_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/x.png")));
        // Reseteo el color de los puntos del jugador
        label_puntos_jugador.setForeground(new java.awt.Color(0, 0, 0));
        label_puntos_maquina.setForeground(new java.awt.Color(1, 88, 0));
        // Cuando ha perdido llamo a la otra interfaz
        if (puntosMaquina == 3) {
            Interface_perdedor.menu();
            Interface.this.setVisible(false);
        }
    }
    
    
    // Empate
    void empate() {
        // Pongo la imagen de la x.png a los dos
        label_check_maquina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/x.png")));
        label_check_jugador.setIcon(new javax.swing.ImageIcon(getClass().getResource("/piedrapapeltijera_grafico/imagenes/x.png")));
        // Reseteo los colores de los dos lados
        label_puntos_jugador.setForeground(new java.awt.Color(0, 0, 0));
        label_puntos_maquina.setForeground(new java.awt.Color(0, 0, 0));
        
    }
    
        
    // Funcion a la que llaman cuando quieren volver a jugar
    static void jugarDeNuevo() {
        int puntos = 0;
        int puntosMaquina = 0;
        menu();

    }
    
    


    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Label_vs;
    private javax.swing.JMenuItem MenuItemAcercaDe;
    private javax.swing.JMenuItem MenuItemCerrar;
    private javax.swing.JMenuItem MenuItemLicencia;
    private javax.swing.JButton jButtonPapel;
    private javax.swing.JButton jButtonPapel2;
    private javax.swing.JButton jButtonPiedra;
    private javax.swing.JButton jButtonPiedra2;
    private javax.swing.JButton jButtonTijera;
    private javax.swing.JButton jButtonTijera2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel label_check_jugador;
    private javax.swing.JLabel label_check_maquina;
    private javax.swing.JLabel label_puntos_jugador;
    private javax.swing.JLabel label_puntos_maquina;
    private javax.swing.JLabel label_result_jugador;
    private javax.swing.JLabel label_result_maquina;
    // End of variables declaration//GEN-END:variables
}
