package com.company;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Main {

    static Scanner scanner = new Scanner(System.in);
    static int contadorID = 0;

    static class Producto {
        int idProducto;
        String nombreProducto;
        float precioProducto;
        String paisProducto;
        LocalDate fechaAlta;
        LocalDate fechaCaducidad;
    }

    public static void main(String[] args) {
        // Creo la lista y llamo al menu
        List<Producto> productos = new ArrayList<>();
        menu(productos);

    }

    // Menu
    static void menu(List<Producto> productos) {
        // Variables
        int opcion;

        clear();
        System.out.println(" M E R C A D O N A");
        System.out.println("-------------------");
        System.out.println("");
        System.out.println("Productos:");
        System.out.println(" 1. Añadir producto.");
        System.out.println(" 2. Retirar producto.");
        System.out.println(" 3. Listar producto.");
        System.out.println(" 4. Actualizar producto.");
        System.out.println(" 0. Salir.");
        System.out.println("");
        System.out.print("Escoge una opcion: ");

        opcion = scanner.nextInt();

        // Añadir productos
        if (opcion == 1) {
            añadirProducto(productos);
        }

        // Listar productos
        else if (opcion == 3) {
            listarProductos(productos);
        }

        // Actualizar producto
        else if (opcion == 4) {
            actualizarProducto(productos);
        }
    }


    // Añadir producto
    static void añadirProducto(List<Producto> productos) {
        Producto producto = new Producto();
        clear();
        // Añadir
        System.out.println("Añadir producto");
        System.out.println("----------------");
        System.out.println("");
        // Nombre
        System.out.print("Escriba el nombre del producto: ");
        producto.nombreProducto = "nivea"; //scanner.nextLine();
        // Precio
        System.out.print("Escriba el precio del producto: ");
        producto.precioProducto = 3.50f; //scanner.nextFloat();
        // Pongo este nextline para separar el enter
//        scanner.nextLine();
        // Pais
        System.out.print("Escriba el pais del producto: ");
        producto.paisProducto = "es"; //scanner.nextLine();
        // Fecha alta
        producto.fechaAlta = LocalDate.now();
        // Fecha caducidad
        System.out.print("Escrina la fecha de caducidad del producto (DD/MM/AAAA): ");
        String fecha = "21/10/2001"; //scanner.next();
        producto.fechaCaducidad = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("d/M/y"));

        // Obtengo el ID del producto
        contadorID++;
        producto.idProducto = contadorID;

        // Añado los productos a la lista
        productos.add(producto);
        menu(productos);

    }


    // Listar productos
    static void listarProductos(List<Producto> productos) {
        Producto producto = new Producto();
        // Variables
        int idBuscar = 0;
        String nombreBuscar = "";
        float precioBuscar = 0.0f;
        String paisBuscar = "";
        boolean existe = false;

        clear();
        // Menu para escojer
        System.out.println(" Listar  productos ");
        System.out.println("-------------------");
        System.out.println("");
        System.out.println("Filtros para listar:");
        System.out.println(" 1. Por ID.");
        System.out.println(" 2. Por nombre.");
        System.out.println(" 3. Por precio.");
        System.out.println(" 4. Por pais.");
        System.out.println(" 0. Salir.");
        System.out.println("");
        System.out.print("Escoge una opcion: ");

        int opcion = scanner.nextInt();

        System.out.println("");
        // Opciones para buscar
        if (opcion == 1) {
            System.out.print("Escribe el ID del producto que deseas buscar: ");
            idBuscar = scanner.nextInt();
        } else if (opcion == 2) {
            System.out.print("Escribe el nombre del producto que deseas buscar: ");
            // Depuro la linea de despues del "int"
            scanner.nextLine();
            nombreBuscar = scanner.nextLine();
        } else if (opcion == 3) {
            System.out.print("Escribe el precio de los productos que deseas buscar: ");
            precioBuscar = scanner.nextFloat();
        } else if (opcion == 4 ) {
            System.out.print("Escribe el pais de los productos que deseas buscar: ");
            // Depuro la linea de despues del "int"
            scanner.nextLine();
            paisBuscar = scanner.nextLine();
        }
        // Salir
        else {
            menu(productos);
        }

        // Busco entre todos los datos de la lista
        for (int i = 0; i < productos.size(); i++) {
            // Busqueda por ID
            if (opcion == 1) {
                if (productos.get(i).idProducto == idBuscar) {
                    // Llamo a esta funcion para que imprima por pantalla los datos del producto
                    mostrarListaFiltrada(productos, i);
                    existe = true;
                }
            }
            // Busqueda por nombre
            else if (opcion == 2) {
                if (productos.get(i).nombreProducto.equals(nombreBuscar)) {
                    mostrarListaFiltrada(productos, i);
                    existe = true;
                }

            }
            // Busqueda por precio
            else if (opcion == 3) {
                if (productos.get(i).precioProducto == precioBuscar) {
                    mostrarListaFiltrada(productos, i);
                    existe = true;
                }
            }
            // Busqueda por pais
            else if (opcion == 4) {
                if (productos.get(i).paisProducto.equals(paisBuscar)) {
                    mostrarListaFiltrada(productos, i);
                    existe = true;
                }
            }
        }

        // Si no existe el producto buscado
        if (!existe) {
            System.out.println("Error!");
            System.out.println("No existen productos con esos criterios de busqueda.");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            listarProductos(productos);
        }

        System.out.print("Escribe un 0 para volver al menu: ");
        scanner.nextInt();
        menu(productos);

    }

    // Funcion para mostrar la lista una vez tenemos los datos del producto que hemos buscado
    private static void mostrarListaFiltrada(List<Producto> productos, int i) {
        clear();
        System.out.println(" P R O D U C T O");
        System.out.println("-------------------------------------");
        System.out.println("| ID: " + productos.get(i).idProducto);
        System.out.println("| Nombre: " + productos.get(i).nombreProducto);
        System.out.println("| Precio: " + productos.get(i).precioProducto);
        System.out.println("| Pais: " + productos.get(i).paisProducto);
        System.out.println("| Fecha de alta: " + productos.get(i).fechaAlta);
        System.out.println("| Fecha de caducidad: " + productos.get(i).fechaCaducidad);
        System.out.println("-------------------------------------");

    }


    // Actualizar producto
    private static void actualizarProducto(List<Producto> productos) {
        clear();
        int posicionProducto = 0;
        System.out.println("Actualizar producto");
        System.out.println("--------------------");
        System.out.print("Escribe el ID del producto que deseas actualizar: ");
        int idActualizar = scanner.nextInt();

        // Busco el producto con ese if
        for (int i = 0; i < productos.size(); i++) {
            if (productos.get(i).idProducto == idActualizar) {
                mostrarListaFiltrada(productos, i);
                posicionProducto = i;
            }
        }

        // Menu actualizar
        System.out.println("");
        System.out.println("Actualizar producto: ");
        System.out.println("-----------------------");
        System.out.println("1. Actualizar nombre");
        System.out.println("2. Actualizar precio");
        System.out.println("3. Actualizar pais");
        System.out.println("");
        System.out.print("Escoge el numero que corresponde a los datos que quieres actualizar: ");
        int opcion = scanner.nextInt();

        // Actualizar nombre
        if (opcion == 1) {
            scanner.nextLine();
            System.out.print("Escriba el nuevo nombre del producto: ");
            productos.get(posicionProducto).nombreProducto = scanner.nextLine();
        }
        // Precio
        else if (opcion == 2) {
            System.out.print("Escriba el nuevo precio del producto: ");
            productos.get(posicionProducto).precioProducto = scanner.nextFloat();
        }
        // Pais
        else if (opcion == 3) {
            scanner.nextLine();
            System.out.print("Escriba el nuevo pais del producto: ");
            productos.get(posicionProducto).paisProducto = scanner.nextLine();
        }

        menu(productos);

    }

    // Clear
    static void clear() {
        for (int i = 0; i < 30 ; i++) {
            System.out.println("");
        }
    }

}
