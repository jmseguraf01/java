package com.company;

import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static boolean exit = false;

    public static void main(String[] args) {
        ConnectDB connectDB = new ConnectDB();
        connectDB.connect();

        selectOptions(connectDB);

        connectDB.close();
    }

    static void selectOptions(ConnectDB connectDB) {
        while (!exit) {
            System.out.println("");
            System.out.println("Connection into database.");
            System.out.println("-------------------");
            System.out.println("1. Create table");
            System.out.println("2. Insert data");
            System.out.println("3. Get all data");
            System.out.println("4. Consulta personalizada");
            System.out.println("0. Exit");

            System.out.print("Selecciona opcion: ");
            int opcion = scanner.nextInt();

            if (opcion == 1) {
                connectDB.createTable();
            } else if (opcion == 2) {
                connectDB.insertData();
            } else if (opcion == 3 ) {
                connectDB.getData();
            } else if (opcion == 4) {
                consultaPersonalizada(connectDB);
            } else if (opcion == 0) {
                exit = true;
            }

        }
    }

    static void consultaPersonalizada(ConnectDB connectDB) {
        System.out.println("Consulta personalizada");
        System.out.println("----------------------");
        System.out.println("1. Consultar por nombre");
        System.out.println("2. Consultar por puntos");
        System.out.println("");
        System.out.print("Selecciona opcion: ");
        int opcion = scanner.nextInt();
        System.out.println("");

        if (opcion == 1) {
            scanner.nextLine();
            System.out.print("Escribe el nombre de la persona que quieres consultar: ");
            String nombre = scanner.nextLine();
            connectDB.getDataName(nombre);
        }

        else if (opcion == 2) {
            System.out.print("Escribe los puntos que quieres consultar: ");
            int puntos = scanner.nextInt();
            connectDB.getDataPoints(puntos);
        }



    }
}
