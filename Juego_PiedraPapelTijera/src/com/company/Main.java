package com.company;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class Main {

    // Menu
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opcion = -1;
        int opcionJugador2 = 0;
        String opcionEscogida = "";
        String opcionEscogidaJugador2 = "";
        int vidasJugador1 = 3;
        int vidasMaquina = 3;
        String nombre;

        Clear();
        System.out.println("Bienvenido al juego de piedra papel y tijera.");
        System.out.println("---------------------------------------------");
        System.out.println("Dispones de 3 vidas, igual que la maquina. Veremos si eres capaz de ganarle!");
        System.out.print("\nEscribe tu nombre: ");
        nombre = scanner.nextLine();

        // Siempre que no se escoge salir, se repite
        while (opcion != 0 && vidasJugador1 > 0 && vidasMaquina > 0) {
            // Variables
            boolean gana = false;
            boolean empate = false;
            // Clear and Menu
            Clear();
            System.out.println("----------------------------------");
            System.out.println("    Piedra / Papel / Tijera    ");
            System.out.println("---------------------------------");
            System.out.println("");
            System.out.println("Vidas:");
            System.out.println(nombre + ": " + vidasJugador1 + "\t\tMaquina: " + vidasMaquina);
            System.out.println("-------------------------------------");
            System.out.println("");
            System.out.println("\t1. Piedra   ");
            System.out.println("\t2. Papel   ");
            System.out.println("\t3._Tijera  ");
            System.out.println("\t0. SALIR   ");
            System.out.println("");
            System.out.print("Escoge una opcion para empezar a jugar: ");
            opcion = scanner.nextInt();

            // Exit
            if (opcion == 0) {
                break;
            }

            // Piedra
            if (opcion == 1) {
                opcionEscogida = "PIEDRA";
            }
            // Papel
            else if (opcion == 2) {
                opcionEscogida = "PAPEL";
            }
            // Tijera
            else if (opcion == 3) {
                opcionEscogida = "TIJERA";
            }
            // Print
            System.out.println("");
            System.out.println("");
            System.out.println(nombre + ": " + opcionEscogida);
            System.out.println("\tVS");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Cojo una opcion aleatoria como segundo jugador
            opcionJugador2 = (int) (Math.random() * 3 + 1);
            // Compruebo jugador 2
            if (opcionJugador2 == 1) {
                opcionEscogidaJugador2 = "PIEDRA";
            } else if (opcionJugador2 == 2) {
                opcionEscogidaJugador2 = "PAPEL";
            } else if (opcionJugador2 == 3) {
                opcionEscogidaJugador2 = "TIJERA";
            }
            System.out.println("Maquina: " + opcionEscogidaJugador2);
            System.out.println("");

            // Calculo los resultados
            // Piedra
            if (opcion == 1) {
                // Gana (Piedra vs tijera)
                if (opcionJugador2 == 3){
                    gana = true;
                }
                // Pierde (piedra vs papel)
                else if (opcionJugador2 == 2) {
                    gana = false;
                }
                // Empate (piedra vs piedra)
                else if (opcionJugador2 == 1) {
                    empate = true;
                }
            }

            // Papel
            else if (opcion == 2) {
                // Gana
                if (opcionJugador2 == 1) {
                    gana = true;
                }
                // Pierde
                else if (opcionJugador2 == 3) {
                    gana = false;
                }
                // Empate
                else if (opcionJugador2 == 2) {
                    empate = true;
                }
            }

            // Tijera
            else if (opcion == 3) {
                // Gana
                if (opcionJugador2 == 2) {
                    gana = true;
                }
                // Pierde
                else  if (opcionJugador2 == 1) {
                    gana = false;
                }
                // Empate
                else if (opcionJugador2 == 3) {
                    empate = true;
                }
            }

            // Punto para el jugador
            System.out.println("---------------------");
            if (gana == true && empate == false) {
                System.out.println("\u001B[32m Maquina: -1 vida");
                vidasMaquina--;
            }
            // Punto para la maquina
            else if (gana == false && empate == false) {
                System.out.println("\u001B[31m" + nombre + ": " + "-1 vida");
                vidasJugador1--;
            }
            // Empate
            else if (empate == true) {
                System.out.println("\u001B[33mEMPATE TECNICO!");
            }
            System.out.println("\u001B[0m---------------------");
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // Has perdido
        if (vidasJugador1 == 0) {
            System.out.println("");
            System.out.println("\u001B[31mHAS PERDIDO CONTRA LA MAQUINA!");
            System.out.println("");
        }
        // Has ganado
        else if (vidasMaquina == 0) {
            System.out.println("");
            System.out.println("\u001B[32mLE HAS GANADO A LA MAQUINA!");
            System.out.println("");
        }
    }

    // Clear
    private static void Clear() {
        for (int i = 0; i < 35 ; i++) {
            System.out.println("");
        }
    }

}
